package com.vectornoid.game;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.vectornoid.tapdoor.statemachine.State;
import com.vectornoid.tapdoor.statemachine.StateListener;
import com.vectornoid.tapdoor.statemachine.StateMachine;
import com.vectornoid.tapdoor.system.Globals;

/**
 * @author void
 * 
 * This is a generic game class that can be used as starting point for state machine based applications. 
 *
 */
abstract public class Game extends StateMachine implements ApplicationListener, StateListener {
	
	@Override
	public void create() {
		Gdx.app.log("Game", "Application started.");
		addListener(this);	
		initialize();
	}
	
	/**
	 * Initialize is called once the application has been started.
	 */
	abstract protected void initialize();

	@Override
	public void dispose() {
		finish();
		Gdx.app.log("Game", "Application terminated.");
	}
	
	@Override
	public void render() {
		
		Gdx.gl.glClearColor(0,0,0,1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
		
		update();
		Globals.update();
	}
	
	@Override
	public void resize(int width, int height) {
		
		Globals.invokeApplicationResizeEvent(width, height);
		
	}
	
	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void stateSwitched(State from, State to) {
		Gdx.app.log("Game", "Game state changed.");
	}
	
	
	
}
