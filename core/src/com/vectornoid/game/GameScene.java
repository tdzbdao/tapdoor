package com.vectornoid.game;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.g3d.environment.DirectionalLight;
import com.vectornoid.tapdoor.gfx.scene3d.Scene;
import com.vectornoid.tapdoor.gfx.scene3d.SceneNode;
import com.vectornoid.tapdoor.statemachine.State;

/**
 * 
 * @author void
 *
 * A game scene is a state in a state driven application.
 * @see Game class
 *
 */

public class GameScene extends Scene implements State, ApplicationListener {
	
	@Override
	public void start() {

		/**
		 * Create a simple perspective camera
		 */
		PerspectiveCamera cam = new PerspectiveCamera(67.0f, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		cam.position.set(10,10,10);
		cam.lookAt(0,0,0);
		
		cam.near = 0.1f;
		cam.far = 100.0f;
			
		/**
		 * Now create the scene and pass the camera.
		 */
		setCamera(cam);
		
		/**
		 * Set some light.
		 */
		getEnvironment().add(new DirectionalLight().set(0.8f, 0.8f, 0.8f, -1f, -0.8f, -0.2f));
		
		/**
		 * Get a handle to the root scene node and create a test cube node.
		 */
		
	/*	SceneNode root = getRootSceneNode();
		
		SceneNode cube = new CubeNode(root);
		cube.setScale(new Vector3(1f,1,1));
		cube.setVisible(true);*/
		
	}

	@Override
	public void finish() {

		
	}

	/**
	 * Updates the camera and renders the scene.
	 */
	@Override
	public void update() {
			
		getCamera().viewportWidth = Gdx.graphics.getWidth();
		getCamera().viewportHeight = Gdx.graphics.getHeight();
		
		prepareFrame();
		renderFrame();
		endFrame();
		
	}

	@Override
	public void create() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resize(int width, int height) {
		
	}

	@Override
	public void render() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

}
