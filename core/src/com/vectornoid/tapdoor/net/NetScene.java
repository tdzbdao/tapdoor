package com.vectornoid.tapdoor.net;

import com.vectornoid.tapdoor.gfx.scene3d.SceneListener;
import com.vectornoid.tapdoor.gfx.scene3d.SceneNode;

public class NetScene implements SceneListener {

	private Server mServer;

	@Override
	public void nodeBoundsChanged(SceneNode n) {
				
	}

	@Override
	public void nodeInserted(SceneNode n) {
		Packet p = new Packet(0x10);
		p.write(n.getID());
		p.write(n);
	}

	@Override
	public void nodeMoved(SceneNode n) {

		Packet p = new Packet(0x11);
		
		p.write(n.getID());
		
		p.write(n.getPosition().x);
		p.write(n.getPosition().y);
		p.write(n.getPosition().z);
		
	}

	@Override
	public void nodeRemoved(SceneNode n) {
		
		Packet p = new Packet(0x12);
		p.write(n.getID());
	}

	@Override
	public void nodeVisibilityChanged(SceneNode n) {
		
		Packet p = new Packet(0x13);
		p.write(n.getID());
		p.write((short)(n.isVisible() ? 1 : 0));
		
		
	}
	
	public NetScene(Server server) {
		mServer = server;
	}

}
