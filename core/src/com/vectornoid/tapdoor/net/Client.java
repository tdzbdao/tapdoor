package com.vectornoid.tapdoor.net;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.util.LinkedList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Net.Protocol;
import com.badlogic.gdx.net.Socket;
import com.badlogic.gdx.net.SocketHints;
import com.badlogic.gdx.utils.ByteArray;

/**
 * Game Client. 
 * 
 * In strict mode an android device will not accept any network polling within the main thread.
 * This class solves the problem by providing an internal polling thread. Updates can still be performed within the main thread.
 * 
 * @author void (tdzbdao@gmail.com)
 *
 */

abstract public class Client {

	Socket mSocket;
	ByteArray mUnfinishedPacket = new ByteArray(0);
	boolean mDone = false;
	private Thread mThread;
	private LinkedList<Packet> mReceivedPackets = new LinkedList<>();
	private InputStream mInputStream;
	private OutputStream mOutputStream;

	public boolean connect(String host, int port) {

		disconnect();
		mDone = false;
		SocketHints hints = new SocketHints();
		hints.receiveBufferSize=1024;

		mSocket = Gdx.net.newClientSocket(Protocol.TCP, host, port, hints);
		mInputStream = mSocket.getInputStream();
		mOutputStream = mSocket.getOutputStream();

		mThread = new Thread(new Runnable() {
			@Override
			public void run() {
				while (!mDone) {
					if (!updateThread())
						break;
				}

			}
		});

		mThread.start();

		return true;
	}

	public boolean isConnected() {
		return mSocket.isConnected();
	}

	public void disconnect() {
		mDone = true;
		if (mSocket != null) {
			mSocket.dispose();
			mSocket = null;
		}
	}

	public void send(Packet pack) throws IOException {
		if (!isConnected())
			throw new IOException("Not connected to server.");

		mOutputStream.write(ByteBuffer.allocate(4).putInt(pack.getBuffer().size).array());
		mOutputStream.write(pack.getBuffer().toArray());

	}

	protected boolean updateThread() {
		try {
			synchronized (this) {
				
				if (mInputStream.available() > 0) {
										
					byte data[] = new byte[mInputStream.available()];
					mInputStream.read(data); //, 0, mSocket.getInputStream().available());
					
					LinkedList<Packet> packets = mReceivedPackets;
					PacketStreamReader.read(data, packets, mUnfinishedPacket);
				}

			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		return true;

	}

	public void update() {
		
		synchronized (mThread) {
			mThread.notify();
			
			while(!mReceivedPackets.isEmpty()) {
				Packet pack = mReceivedPackets.poll();
				onPacket(pack);
			}
			
		}

		
	}

	abstract protected void onPacket(Packet p);

}
