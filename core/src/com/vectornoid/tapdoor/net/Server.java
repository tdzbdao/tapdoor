package com.vectornoid.tapdoor.net;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map.Entry;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Net.Protocol;
import com.badlogic.gdx.net.ServerSocket;
import com.badlogic.gdx.net.ServerSocketHints;
import com.badlogic.gdx.net.Socket;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ByteArray;
import com.badlogic.gdx.utils.Disposable;

abstract public class Server implements Disposable {

	ServerSocket mSocket;
	Thread mAcceptThread;
	Thread mUpdateThread;
	boolean mDone = false;

	private class SocketInfo {
		public SocketUserData userData = null;
		public ByteArray unfinishedPacket = new ByteArray();
	}

	HashMap<Socket, SocketInfo> mConnections = new HashMap<>();

	public Server(final int port) {
		ServerSocketHints hints = new ServerSocketHints();
		mSocket = Gdx.net.newServerSocket(Protocol.TCP, port, hints);

		mAcceptThread = new Thread(new Runnable() {
			@Override
			public void run() {
				Gdx.app.log("Server", "Listening on port " + port + ".");
				startAccept();
			}
		});

		mAcceptThread.start();

		mUpdateThread = new Thread(new Runnable() {
			@Override
			public void run() {
				while (!mDone) {
					update();
				}
			}
		});

		mUpdateThread.start();
	}

	private void startAccept() {
		// wait for the next client connection
		Socket client = null;
		while (client == null) {
			try {
				client = mSocket.accept(null);
			} catch (Exception e) {
			}
		}

		SocketUserData data = onConnect(client);
		SocketInfo info = new SocketInfo();
		info.userData = data;

		synchronized (this) {
			mConnections.put(client, info);
		}

		Gdx.app.log("Server", "New client connected from " + client.getRemoteAddress() + ".");

		startAccept();
	}

	public static void send(Socket s, Packet pack) throws IOException {
		if (!s.isConnected())
			throw new IOException("Not connected to server.");

		s.getOutputStream().write(ByteBuffer.allocate(4).putInt(pack.getBuffer().size).array());
		s.getOutputStream().write(pack.getBuffer().toArray());

	}

	public void dispose() {
		mDone = true;
	}

	public void update() {

		Array<Socket> disconnections = new Array<Socket>();
		synchronized (this) {

			for (Entry<Socket, SocketInfo> es : mConnections.entrySet()) {
				Socket s = es.getKey();
				SocketInfo si = es.getValue();
				try {
					if (s.getInputStream().available() > 0) {

						byte data[] = new byte[s.getInputStream().available()];
						s.getInputStream().read(data, 0, s.getInputStream().available());

						LinkedList<Packet> packets = new LinkedList<>();
						PacketStreamReader.read(data, packets, si.unfinishedPacket);
						for (Packet p : packets) {
							// Process packets.
							onPacket(s, si.userData, p);
						}

					} else if (!s.isConnected()) {

						Gdx.app.log("Server", "Client " + s.getRemoteAddress() + " disconnected.");
						onDisconnect(s, si.userData);
						disconnections.add(s);
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			for (Socket s : disconnections) {
				mConnections.remove(s);
			}

		}

	}

	protected abstract void onPacket(Socket s, SocketUserData userData, Packet p);

	protected abstract void onDisconnect(Socket s, SocketUserData userData);

	protected abstract SocketUserData onConnect(Socket s);

}
