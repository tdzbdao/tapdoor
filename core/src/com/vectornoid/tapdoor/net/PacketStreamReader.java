package com.vectornoid.tapdoor.net;

import java.util.LinkedList;

import com.badlogic.gdx.utils.ByteArray;

public class PacketStreamReader {

	/**
	 * Reads packets from the given byte array. Fully supports packet fragmentation.
	 * 
	 * @param input
	 * @param packets
	 */

	public static void read(byte[] input, LinkedList<Packet> packets, ByteArray unfinishedPacket) {
		
		int offset = 0;
		int len = 0;
		
		if (unfinishedPacket.size != 0) { // Got unfinished buffered data.

			if (unfinishedPacket.size < 4) { // Fill up the length bytes
				unfinishedPacket.addAll(input, 0, Math.min(input.length, 4 - unfinishedPacket.size));
			}

			if (unfinishedPacket.size >= 4) { // We got at least the length of the packet
				len = ((unfinishedPacket.items[0] & 0xFF) << 24) | ((unfinishedPacket.items[1] & 0xFF) << 16) | ((unfinishedPacket.items[2] & 0xFF) << 8) | (unfinishedPacket.items[3] & 0xFF);
				offset = len - (unfinishedPacket.size - 4);
				
				unfinishedPacket.addAll(input, 0, Math.min(input.length, offset));
				
				if (unfinishedPacket.size == len + 4) { // Got it.
					
					Packet pack = new Packet(unfinishedPacket.items, 4, unfinishedPacket.size - 4);
					packets.add(pack);
					unfinishedPacket.clear();
					
				}
			} else
				// Still reading length bytes, do nothing.
				return;
		}

		while (offset < input.length) { // As long as no unfinished packet was completed this step, offset is always zero.
			if (offset + 4 <= input.length) {
				len = ((input[offset] & 0xFF) << 24) | ((input[offset + 1] & 0xFF) << 16) | ((input[offset + 2] & 0xFF) << 8) | (input[offset + 3] & 0xFF);
				if (len == 0) // Illegal length. This should terminate the connection.
					break;
				offset += 4; // Strip the length

				if (offset + len <= input.length) {
					Packet pack = new Packet(input, offset, len);
					packets.add(pack);
				} else {
					unfinishedPacket.addAll(input, offset - 4, input.length - offset + 4);
				}
			} else { // Note that unfinishedPacket will also hold the packet length!
				unfinishedPacket.addAll(input, offset, input.length - offset);
				break;
			}

			offset += len; // Move on to the next packet.
		}

	}

}
