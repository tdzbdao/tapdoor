package com.vectornoid.tapdoor.net;

import java.nio.ByteBuffer;

import com.badlogic.gdx.utils.ByteArray;
import com.vectornoid.tapdoor.system.Serializable;
import com.vectornoid.tapdoor.system.SerializedReader;
import com.vectornoid.tapdoor.system.SerializedWriter;

public class Packet {
	
	ByteArray mBuffer = new ByteArray();
	int mCursor = 2;

	public Packet(int id) {
		write((short)id);
	}
	
	public Packet(byte[] data) {
		mBuffer.addAll(data);
	}
	
	public Packet(byte[] data, int offset, int length) {
		mBuffer.addAll(data, offset, length);
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "Packet[" + getId() + "](l=" + getLength() + ")";
	}

	public int getLength() {
		return mBuffer.size - 2;
	}

	public short getId() {
		return (short)( ((mBuffer.get(1)&0xFF)<<8) | (mBuffer.get(0)&0xFF) );
	}
	
	public void write(byte[] bytes) {
		mBuffer.addAll(bytes);		
	}
	
	public void write(byte[] bytes, int offset, int len) {
		mBuffer.addAll(bytes, offset, len);		
	}

	public void write(short x) {
		mBuffer.addAll(ByteBuffer.allocate(2).putShort(x).array());
	}

	public void write(int x) {
		mBuffer.addAll(ByteBuffer.allocate(4).putInt(x).array());
	}
	
	public void write(long x) {
		mBuffer.addAll(ByteBuffer.allocate(8).putLong(x).array());	
	}
	
	public void write(float x) {
		mBuffer.addAll(ByteBuffer.allocate(4).putFloat(x).array());
	}
	
	public void write(double x) {
		mBuffer.addAll(ByteBuffer.allocate(8).putDouble(x).array());
	}

	public void write(String x) {
		write(x.getBytes().length);
		mBuffer.addAll(x.getBytes());
	}
		
	public void readBytes(byte[] bytes, int offset, int len) {
		for(int i=0;i<len;i++) {
			bytes[offset + i] = mBuffer.items[mCursor++];
		}
	}
	
	public short readShort() {
		short r = (short)( ((mBuffer.get(mCursor+1)&0xFF)<<8) | (mBuffer.get(mCursor)&0xFF) );
		mCursor += 2;
		return r;
	}
	
	public int readInt() {
		int r = ((mBuffer.get(mCursor+0)&0xFF)<<24) | ((mBuffer.get(mCursor+1)&0xFF)<<16) | ((mBuffer.get(mCursor+2)&0xFF)<<8) |(mBuffer.get(mCursor+3)&0xFF);
		mCursor += 4;
		return r;
	}
	
	public float readFloat() {
		float r = ((mBuffer.get(mCursor+0)&0xFF)<<24) | ((mBuffer.get(mCursor+1)&0xFF)<<16) | ((mBuffer.get(mCursor+2)&0xFF)<<8) |(mBuffer.get(mCursor+3)&0xFF);
		mCursor += 4;
		return r;
	}
	
	public long readLong() {
		long r = ((mBuffer.get(mCursor+0)&0xFF)<<56) | ((mBuffer.get(mCursor+1)&0xFF)<<48) | ((mBuffer.get(mCursor+2)&0xFF)<<40) | ((mBuffer.get(mCursor+3)&0xFF)<<32) | ((mBuffer.get(mCursor+4)&0xFF)<<24) | ((mBuffer.get(mCursor+5)&0xFF)<<16) | ((mBuffer.get(mCursor+6)&0xFF)<<8) | (mBuffer.get(mCursor+7)&0xFF);
		mCursor += 8;
		return r;
	}
	
	public double readDouble() {
		double r = ((mBuffer.get(mCursor+0)&0xFF)<<56) | ((mBuffer.get(mCursor+1)&0xFF)<<48) | ((mBuffer.get(mCursor+2)&0xFF)<<40) | ((mBuffer.get(mCursor+3)&0xFF)<<32) | ((mBuffer.get(mCursor+4)&0xFF)<<24) | ((mBuffer.get(mCursor+5)&0xFF)<<16) | ((mBuffer.get(mCursor+6)&0xFF)<<8) | (mBuffer.get(mCursor+7)&0xFF);
		mCursor += 8;
		return r;
	}
	
	public String readString() {
		int len = readInt();
		
		String s = new String(mBuffer.items, mCursor, len);
		mCursor += len;
		return s;
	}

	public ByteArray getBuffer() {
		return mBuffer;
	}

	public void clear() {
		short id = getId();
		mBuffer.clear();
		write(id);
		
		mCursor = 2;
	}
	
	private class SerializedPacketWriter extends SerializedWriter {
		private Packet mPacket;
		
		public SerializedPacketWriter(Packet p) {
			mPacket = p;
		}

		@Override
		public void write(byte[] data, int offset, int len) {
			mPacket.write(data, offset, len);
		}
	}
	
	private class SerializedPacketReader extends SerializedReader {
		private Packet mPacket;
		
		public SerializedPacketReader(Packet p) {
			mPacket = p;
		}

		@Override
		public void read(byte[] target, int offset, int len) {
			mPacket.readBytes(target,  offset,  len);
		}

		
	}
		
	public void write(Serializable s) {
		s.serialize(new SerializedPacketWriter(this));		
	}
	
	public void readSerialized(Serializable s) {
		s.unserialize(new SerializedPacketReader(this));
	}
		
}
