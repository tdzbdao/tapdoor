package com.vectornoid.tapdoor.l10n;

import java.util.ArrayList;

public class LocalizationManager {

	private ArrayList<LocalizationProvider> mProviders = new ArrayList<>();

	public void addProvider(LocalizationProvider p) {
		mProviders.add(0, p); // Add at front to increase priority.
	}

	public String getString(String source) {
		return getString(source, "");
	}

	public String getString(String source, String group) {
				
		for (LocalizationProvider p : mProviders) {
			String r = p.localize(source, group);
			if(r != null)
				return r;
		}
		return source;
	}
}
