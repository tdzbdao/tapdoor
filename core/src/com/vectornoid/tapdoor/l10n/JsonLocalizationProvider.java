package com.vectornoid.tapdoor.l10n;

import java.util.HashMap;

import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.Json.Serializable;
import com.badlogic.gdx.utils.JsonValue;

public class JsonLocalizationProvider implements LocalizationProvider {

	private HashMap<String, HashMap<String, String>> mGroups = new HashMap<>();

	private static class Serializer implements Serializable {
		private JsonValue mJsonData;

		@Override
		public void write(Json json) {
			// We don't need this.
		}

		@Override
		public void read(Json json, JsonValue jsonData) {

			mJsonData = jsonData;
		}

		void apply(JsonLocalizationProvider p) {
			for (int i = 0; i < mJsonData.size; ++i) {
				JsonValue c = mJsonData.get(i);
				
				if (c.isArray()) {
					HashMap<String, String> g = new HashMap<>();
					for (int j = 0; j < c.size; ++j) {
						JsonValue cc = c.get(i);
						if (cc.isString()) {
							g.put(cc.name, cc.asString());
						}
					}
					p.mGroups.put(c.name, g);

				} else if (c.isString()) {
					p.mGroups.get("").put(c.name, c.asString());
				}
			}

		}

	}

	public JsonLocalizationProvider(String data) {

		mGroups = new HashMap<>();
		mGroups.put("", new HashMap<String, String>());
		
		Json json = new Json();
		Serializer s = json.fromJson(Serializer.class, data);
		s.apply(this);
			}

	@Override
	public String localize(String source, String group) {
		
		if(group == null)
			group = "";
		
		
		if(!mGroups.containsKey(group))
			return null;
		
		HashMap<String, String> g = mGroups.get(group);
		
		if(!g.containsKey(source))
			return null;
				
		return g.get(source);
	}

}
