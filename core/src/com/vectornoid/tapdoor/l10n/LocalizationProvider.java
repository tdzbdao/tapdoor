package com.vectornoid.tapdoor.l10n;

public interface LocalizationProvider {

	public String localize(String source, String group);
	
}
