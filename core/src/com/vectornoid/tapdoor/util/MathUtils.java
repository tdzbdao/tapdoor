package com.vectornoid.tapdoor.util;

import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.math.collision.Ray;

public class MathUtils {

	public static float Epsilon = 0.00001f;

	private MathUtils() {
	}

	/**
	 * Java implementation of Paul Bourkes shortest line algorithm.
	 * @see http://paulbourke.net/geometry/pointlineplane/
	 * @see http://paulbourke.net/geometry/pointlineplane/lineline.c
	 * 
	 * @param r1 
	 * @param r2
	 * @param pa Output vector a 
	 * @param pb Output vector b
	 * @return true if a shortest line exists.
	 */
	
	public static boolean shortestLineBetweenRays(Ray r1, Ray r2, Vector3 pa, Vector3 pb) {

		float d1343, d4321, d1321, d4343, d2121;
		float numer, denom;
		float mua, mub;

		Vector3 p1 = r1.origin;
		Vector3 p2 = r1.direction.cpy().scl(1000.0f);
		Vector3 p3 = r2.origin;
		Vector3 p4 = r2.direction.cpy().scl(1000.0f);

		Vector3 p13 = new Vector3();
		Vector3 p43 = new Vector3();
		Vector3 p21 = new Vector3();

		p13.x = p1.x - p3.x;
		p13.y = p1.y - p3.y;
		p13.z = p1.z - p3.z;
		p43.x = p4.x - p3.x;
		p43.y = p4.y - p3.y;
		p43.z = p4.z - p3.z;

		if (Math.abs(p43.x) < Epsilon && Math.abs(p43.y) < Epsilon && Math.abs(p43.z) < Epsilon)
			return false;
		p21.x = p2.x - p1.x;
		p21.y = p2.y - p1.y;
		p21.z = p2.z - p1.z;
		if (Math.abs(p21.x) < Epsilon && Math.abs(p21.y) < Epsilon && Math.abs(p21.z) < Epsilon)
			return false;

		d1343 = p13.x * p43.x + p13.y * p43.y + p13.z * p43.z;
		d4321 = p43.x * p21.x + p43.y * p21.y + p43.z * p21.z;
		d1321 = p13.x * p21.x + p13.y * p21.y + p13.z * p21.z;
		d4343 = p43.x * p43.x + p43.y * p43.y + p43.z * p43.z;
		d2121 = p21.x * p21.x + p21.y * p21.y + p21.z * p21.z;

		denom = d2121 * d4343 - d4321 * d4321;
		if (Math.abs(denom) < Epsilon)
			return false;
		numer = d1343 * d4321 - d1321 * d4343;

		mua = numer / denom;
		mub = (d1343 + d4321 * (mua)) / d4343;

		pa.x = p1.x + mua * p21.x;
		pa.y = p1.y + mua * p21.y;
		pa.z = p1.z + mua * p21.z;
		pb.x = p3.x + mub * p43.x;
		pb.y = p3.y + mub * p43.y;
		pb.z = p3.z + mub * p43.z;

		return true;
	}
}
