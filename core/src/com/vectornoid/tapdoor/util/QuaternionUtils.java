package com.vectornoid.tapdoor.util;

import com.badlogic.gdx.math.Quaternion;
import com.badlogic.gdx.math.Vector3;

/**
 * @author void
 *
 * Additional functionality that is (not yet) present to the libGDX math.Quaternion class.
 *
 */
public class QuaternionUtils {
	
	public static com.badlogic.gdx.math.Quaternion getRotationTo(Vector3 src, Vector3 dest, Quaternion out)
	{
		Vector3 fallbackAxis = new Vector3();
		
		Quaternion q = out;
		
		Vector3 v0 = src.cpy();
		Vector3 v1 = dest.cpy();
		
		v0.nor();
		v1.nor();
		
		float d = v0.dot(v1);
		if(d >= 1.0f)
		{
			return q.idt();
		}
		if(d < (1e-6f - 1.0f))
		{
			if(fallbackAxis.epsilonEquals(0, 0, 0, 0.001f))
			{
				q.setFromAxisRad(fallbackAxis, (float) Math.PI);
			}
			else
			{
				Vector3 axis = Vector3.X.cpy().crs(src);
				if(axis.isZero()) // This axis is colinear, pick another
				{
					axis = Vector3.Y.cpy().crs(src);
				}
				axis.nor();
				q.setFromAxisRad(axis, (float) Math.PI);
			}
		}
		else
		{
			float s = (float) Math.sqrt( (1+d)*2 );
			float invs = 1.0f / s;
			Vector3 c = v0.crs(v1);
			
			q.x = c.x * invs;
			q.y = c.y * invs;
			q.z = c.z * invs;
			q.w = s * 0.5f;
			
			q.nor();
			
		}
		return q;
	}
	
	private QuaternionUtils() {}
	
	
}
