package com.vectornoid.tapdoor.util;

public class ArrayUtils {

	public static float[] replace(float[] array, int offset, int length, float[] replacement) {
		
		float[] in = array;
		
		int delta = replacement.length - length;
		int cutEnd = offset + length;
		
		if(delta > 0) { // Replacement is longer than old data, shift end to the right
			for(int i=in.length-1;i>cutEnd;i-=1) {
				in[i] = in[i-1]; 
			}
		}
		
		if(delta > 0) { // Replacement is short than old data, shift end to the left
			for(int i=cutEnd;i<in.length-1;i+=1) {
				in[i] = in[i+1]; 
			}
		}
	
		for(int i=0; i < replacement.length; ++i) {
			in[i+offset] = replacement[i];
		}
		
		return in;
		
	}
	
	public static short[] replace(short[] array, int offset, int length, short[] replacement) {
		
		short[] in = array;
		
		int delta = replacement.length - length;
		int cutEnd = offset + length;
		
		if(delta > 0) { // Replacement is longer than old data, shift end to the right
			for(int i=in.length;i>cutEnd;i-=1) {
				in[i] = in[i-1]; 
			}
		}
		
		if(delta > 0) { // Replacement is short than old data, shift end to the left
			for(int i=cutEnd;i<in.length-1;i+=1) {
				in[i] = in[i+1]; 
			}
		}
	
		for(int i=0; i < replacement.length; ++i) {
			in[i+offset] = replacement[i];
		}
		
		return in;
		
	}
	
}
