package com.vectornoid.tapdoor.simulation;

import java.util.ArrayList;
import java.util.PriorityQueue;

import com.badlogic.gdx.Gdx;

/**
 * @author void
 *
 * Basic simulation class. Handles simulation ticks and events of the application.
 * 
 */
abstract public class Simulation {

	private float mTickDuration = 0.5f; // 2 ticks per second
	private float mDeltaTime = 0;
	private long mTickCounter = 0;
	private long mTicksPerIntervalIncrease = 10;
	private long mRemainingTicksBeforeIntervalIncrease = mTicksPerIntervalIncrease;
	private long mIntervalStep = 1;
	private long mCurrentTime = 0;
	private TimeInterval mTime;

	private class QueuedEvent implements Comparable<QueuedEvent> {
		public Runnable runnable;
		public long tick;

		@Override
		public int compareTo(QueuedEvent o) {
			return (tick < o.tick) ? -1 : (tick == o.tick ? 0 : 1);
		}

	}

	private PriorityQueue<QueuedEvent> mQueuedEvents = new PriorityQueue<>();

	public Simulation(TimeInterval interval) {
		mTime = interval;
	}

	public void update() {

		mDeltaTime += Gdx.graphics.getDeltaTime();

		while (mDeltaTime >= mTickDuration) {
			mDeltaTime -= mTickDuration;

			if (--mRemainingTicksBeforeIntervalIncrease <= 0) {
				mRemainingTicksBeforeIntervalIncrease = mTicksPerIntervalIncrease;
				mCurrentTime += mIntervalStep;
			}

			updateTick(mTickCounter);

			if (!mQueuedEvents.isEmpty()) {
				QueuedEvent e = mQueuedEvents.peek();
				
				ArrayList<QueuedEvent> tmp = new ArrayList<>();
				while (e.tick <= mTickCounter) {
					mQueuedEvents.remove();
					tmp.add(e);
					
					if(!mQueuedEvents.isEmpty())
						e = mQueuedEvents.peek();
					else
						break;
				}
				
				for(QueuedEvent qe : tmp) {
					qe.runnable.run();
				}
			}

			mTickCounter++;
		}

	}

	public String getTimeString() {
		return mTime.format(mCurrentTime);
	}

	abstract protected void updateTick(long tick);

	public void addEvent(long tickDeltaFromNow, Runnable e) {
		QueuedEvent qe = new QueuedEvent();
		qe.runnable = e;
		qe.tick = mTickCounter + tickDeltaFromNow;
		mQueuedEvents.add(qe);
		
	}
	
	public long getCurrentTime() {
		return mCurrentTime;
	}
	

	public TimeInterval getTimeFormat() {
		return mTime;
	}

}
