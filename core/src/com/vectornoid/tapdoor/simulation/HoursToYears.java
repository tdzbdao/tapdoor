package com.vectornoid.tapdoor.simulation;

public class HoursToYears extends TimeInterval {

	public HoursToYears() {
		super("Hours", "h");
		
		TimeInterval days = new TimeInterval("days",  "D");
		TimeInterval months = new TimeInterval("months",  "M");
		TimeInterval years = new TimeInterval("years",  "Y");
		
		setNextInterval(days, 24);
		days.setNextInterval(months, 30);
		months.setNextInterval(years, 12);
	}
	
}
