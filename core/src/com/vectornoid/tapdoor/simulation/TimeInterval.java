package com.vectornoid.tapdoor.simulation;

public class TimeInterval {

	private String mLongName;
	private String mShortName;
	private TimeInterval mNextInterval;
	private long mLimit = -1;

	public TimeInterval(String longName, String shortName) {
		mLongName = longName;
		mShortName = shortName;
	}

	public void setNextInterval(TimeInterval v, long thisLimit) {
		mNextInterval = v;
		mLimit = thisLimit;
	}
	
	public String getFormattedString(long t) {
		return (mNextInterval == null ? "" + mShortName + t  : " " + mShortName + (t%mLimit)) ;
	}
	
	public String format(long t) {
		if(mNextInterval == null) {
			return getFormattedString(t);
		}
		else {
			return mNextInterval.format((long) Math.floor(t / mLimit)) + getFormattedString(t);
		}	
	}

}
