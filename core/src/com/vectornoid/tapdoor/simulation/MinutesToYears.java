package com.vectornoid.tapdoor.simulation;

public class MinutesToYears extends TimeInterval {

	public MinutesToYears()
	{
		super("Minutes", "m");
				
		setNextInterval(new HoursToYears(), 60);
	}
	
}
