package com.vectornoid.tapdoor.director;

import java.util.HashMap;

import com.badlogic.gdx.utils.Array;

/**
 * A director is responsible for handling a set of timed (co-)linear actions. For example, directors can be used to create cut-scenes or dialogs.
 * 
 * Actions could be either blocking or non-blocking and can define one or more followers, so the director can be used with a semi-parallel set of actions.
 * 
 * EX: Consider the following tree of actions:
 * 
 * <ul>
 * <li><label>ROOT</label>
 * <ul>
 * <li><label>ACTION A</label></li>
 * <li><label>ACTION B</label>
 * <ul>
 * <li><label>ACTION C</label></li>
 * </ul>
 * </li>
 * </ul>
 * </li>
 * </ul>
 * 
 * Now, if A and B would be defined as blocking and A might take more time to finish than B does, C's invokation would be deferred until A has finished.
 * 
 * If A and B would be defined as non-blocking, C's invokation would take process immediately after B finished, regardless of A's current state.
 * 
 * @author void (tdzbdao@gmail.com)
 * 
 */

public class Director {

	private Array<SceneAction> mRunningActions = new Array<SceneAction>();
	HashMap<SceneAction, Array<SceneAction>> mAfterActions = new HashMap<SceneAction, Array<SceneAction>>();
	private Array<SceneAction> mBlockingActions = new Array<SceneAction>();
	SceneAction mRoot = null;

	Array<SceneAction> mRunningActionsCache = new Array<SceneAction>();
	Array<SceneAction> mFinishedActionsCache = new Array<SceneAction>();
	Array<SceneAction> mBlockingActionsCache = new Array<SceneAction>();

	/**
	 * Registers a root action that will be called when the directors update method is fired for the first time.
	 * 
	 * @param a
	 *            The action to call.
	 * @return this action for chaining.
	 */
	public <T extends SceneAction> T begin(T a) {
		a.mDirector = this;
		mRoot = a;
		mRunningActions.add(a);
		return a;
	}

	public void skipAll() {
		while (!performActions(0, true))
			;
	}

	public boolean update(float deltatime) {

		return performActions(deltatime, false);
	}

	/**
	 * Performs the directed actions.
	 * 
	 * @param deltatime
	 *            The current delta time in seconds.
	 * @return TRUE if the director ran out of actions, otherwise FALSE.
	 */
	private boolean performActions(float deltatime, boolean skipAll) {

		if (mRoot != null) {
			mRoot.started();
			mRoot = null;
		}

		Array<SceneAction> runningActions = mRunningActionsCache;
		Array<SceneAction> finishedActions = mFinishedActionsCache;
		Array<SceneAction> blockingActions = mBlockingActionsCache;

		boolean onlyBlocking = true;

		for (SceneAction a : mRunningActions) {

			if (a.mIsBlocking) { // This is a blocking action.
				blockingActions.add(a);
			} else if (!skipAll && !a.update(deltatime)) { // Still running or skipAll
				runningActions.add(a);
			} else { // Action is completed
				finishedActions.add(a);
				a.finished();
			}

			if (!a.mIsBlocking) // At least one of the running actions is
								// non-blocking.
				onlyBlocking = false;
		}

		if (onlyBlocking && blockingActions.size > 0) { // All actions are currently blocking actions.

			mRunningActions.clear();
			mRunningActions.addAll(blockingActions);

			for (SceneAction a : mRunningActions) { // Un-block all actions in
													// order to start them next
													// update.
				a.mIsBlocking = false;
			}
			runningActions.clear();
			finishedActions.clear();
			blockingActions.clear();
			return false;
		}

		else if (blockingActions.size > 0) { // Add all blocking actions to the
												// blocking actions register.
			mBlockingActions.addAll(blockingActions);
		}

		mRunningActions.clear();
		mRunningActions.addAll(mRunningActionsCache);

		for (SceneAction a : finishedActions) { // Iterate over all finished
												// actions.
			if (!a.mIsBlocking || mRunningActions.size == 0) { // Not blocking and no other actions running.
				if (mAfterActions.containsKey(a)) { // This action has a follower.
					Array<SceneAction> ar = mAfterActions.get(a);
					for (SceneAction sa : ar) {
						sa.started(); // Call the followers started event.
					}
					mRunningActions.addAll(ar); // Add all followers to the running actions register.
					mAfterActions.remove(a);
				}
			} else if (a.mIsBlocking) {
				mBlockingActions.add(a);
			}
		}

		if (mRunningActions.size == 0) { // No more running actions ...
			if (mBlockingActions.size == 0) { // ... and no blocking actions, which means we're done.
				runningActions.clear();
				finishedActions.clear();
				blockingActions.clear();

				return true;
			}

			// Move all blocking actions to our running actions register in order to start them on next update.
			mRunningActions.clear();
			mRunningActions.addAll(mBlockingActions);
			mBlockingActions.clear();
		}
		runningActions.clear();
		finishedActions.clear();
		blockingActions.clear();

		return false;

	}

}
