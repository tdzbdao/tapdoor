package com.vectornoid.tapdoor.director;

import com.badlogic.gdx.utils.Array;

/**
 * A SceneAction is any kind of (semi-)blocking action that can be handled by a director.
 * 
 * For example, when playing a dialog, a phrase spoken by one of the characters might be implemented by a SceneAction.
 * 
 * @author void (tdzbdao@gmail.com)
 */

public abstract class SceneAction {

	Director mDirector;
	Array<SceneAction> mNextActions = new Array<SceneAction>();
	boolean mIsBlocking = false;
	boolean mIsDone = false;

	/**
	 * Adds a following action after this SceneAction.
	 * 
	 * If multiple actions were set to start after this one, they will be executed concurrently.
	 * 
	 * @param The
	 *            following action.
	 * @return The following action for chaining.
	 */
	public <T extends SceneAction> T after(T s) {

		if (!mDirector.mAfterActions.containsKey(this))
			mDirector.mAfterActions.put(this, new Array<SceneAction>());
		s.mDirector = mDirector;
		mDirector.mAfterActions.get(this).add(s);
		return s;
	}

	public abstract void finished();

	public abstract void started();

	@SuppressWarnings("unchecked")
	public <T extends SceneAction> T sync() {
		mIsBlocking = true;
		return (T) this;
	}

	public abstract boolean update(float deltatime); // Must return true if finished.
}
