package com.vectornoid.tapdoor.statemachine;

/**
 * 
 * State interface.
 * 
 * @author void (tdzbdao@gmail.com)
 *
 */

public interface State {

	/**
	 * Called after a state transition when a state is starting up.
	 */
	public void start();
	
	/**
	 * Called before a state transition when a state is finishing.
	 */
	public void finish();
	
	/**
	 * Called when a state is currently active in a state machine and no transition is scheduled.
	 */
	public void update();
	
}
