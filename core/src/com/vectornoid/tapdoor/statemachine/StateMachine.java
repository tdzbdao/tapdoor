package com.vectornoid.tapdoor.statemachine;

import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;

import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.ObjectSet;

/**
 * 
 * Recursive state machine.
 * 
 * @author void (tdzbdao@gmail.com)
 *
 */
public class StateMachine implements Disposable, State {
		
	private class Transition {

		public State from, to;

		public Transition(State from, State to) {
			this.from = from;
			this.to = to;
		}

		@Override
		public boolean equals(Object obj) {
			if (obj.getClass() == Transition.class) {
				Transition o = (Transition) obj;
				return o.from.equals(from) && o.to.equals(to);
			}
			return false;
		}

	}

	private ObjectSet<State> mStates = new ObjectSet<>();
	private State mCurrentState = null;
	private ObjectMap<State, ObjectSet<Transition>> mTransitions = new ObjectMap<State, ObjectSet<Transition>>();
	private State mStart;

	private HashMap<State, State> mPredecessors = new HashMap<>();
	private HashMap<State, Integer> mDistances = new HashMap<>();
	private Array<State> mQ = new Array<State>();
	private Deque<State> mPath = new LinkedList<State>();
	
	private TransitionHandler mTransitionHandler = null;
	private Transition mCurrentTransition = new Transition(null, null);
	private HashSet<StateListener> mListeners = new HashSet<>();	

	private class StartState implements State {
		@Override
		public String toString() {
			return "[Start]";
		}

		@Override
		public void start() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void finish() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void update() {
			// TODO Auto-generated method stub
			
		}
	}

	public StateMachine() {

		registerState(new StartState()); // Register the start state.

	}
	
	public void addListener(StateListener listener) {
		mListeners.add(listener);
	}
	
	public void removeListener(StateListener listener) {
		mListeners.remove(listener);
	}

	public State getStartState() {
		return mStart;
	}

	public State registerState(State s) {
		mStates.add(s);

		if (mCurrentState == null) // Make the first registered state the starting state.
		{
			setCurrentState(s);
			mStart = s;
		}

		return s;
	}

	public void setCurrentState(State s) {
		if (!mStates.contains(s))
			throw new IllegalStateException("State is not registered to this machine.");

		mCurrentState = s;

	}

	public void registerTransition(State from, State to) {

		if (!mStates.contains(from) || !mStates.contains(to))
			throw new IllegalStateException("State is not registered to this machine.");

		Transition t = new Transition(from, to);

		ObjectSet<Transition> transitions = null;

		if (!mTransitions.containsKey(from)) {
			transitions = new ObjectSet<>();
			mTransitions.put(from, transitions);
		} else
			transitions = mTransitions.get(from);
		
		if (transitions.contains(t))
			throw new IllegalStateException("State transition redifined.");
		
		transitions.add(t);

	}

	public void switchState(State newState) {
		calculatePath(mCurrentState, newState);
	}
	
	@Override
	public void update() {
		
		if( mCurrentTransition.from != null && mCurrentTransition.to != null) {
			if(mTransitionHandler != null && !mTransitionHandler.update(mCurrentTransition.from, mCurrentTransition.to)) // Update the current transition.
				return;
			
			for(StateListener l : mListeners) {
				l.stateSwitched(mCurrentTransition.from, mCurrentTransition.to);
			}
			
			mCurrentTransition.from = mCurrentTransition.to = null;
			
			mCurrentState.start();
		}
		
		if(mPath.isEmpty()) { // Nothing to do.
			mCurrentState.update();
			return;
		}
				
		State nextState = mPath.poll();
		
		mCurrentState.finish();

		mCurrentTransition.from = mCurrentState;
		mCurrentTransition.to = nextState;
		
		mCurrentState = nextState;
		
		if(mTransitionHandler != null)
			mTransitionHandler.start(mCurrentTransition.from, mCurrentTransition.to);
		
	}
	
	private void calculatePath(State from, State to) {
		
		Deque<State> p = mPath;
		p.clear();
	
		if(from == to) { // Ignore explicit self referencing transition.
			return;
		}
		
		HashMap<State, State> predecessors = mPredecessors;
		HashMap<State, Integer> distances = mDistances;
		Array<State> Q = mQ;

		// Init
		for (State s : mStates) {
			Q.add(s);
			distances.put(s, s == from ? 0 : -1);
			predecessors.put(s, null);
		}
	
		boolean first=true;		

		// Dijkstra
		while (Q.size > 0) {

			int lowestDistance = -1;
			State u = null;
			int d = 0;
			
			for (State s : Q) {
				d = distances.get(s);
				if (lowestDistance < 0 || (d >= 0 && lowestDistance > d)) {
					u = s;
					lowestDistance = d;

				}
			}
			
			if (u == to && !first) { // Target reached
				break;
			}
			
			first=false;

			Q.removeValue(u, true);
			
			if (mTransitions.containsKey(u)) {
				for (Transition t : mTransitions.get(u)) {
					State v = t.to;

					if (Q.contains(v, true)) {
						
						// Distance update
						int alternative = distances.get(u) + 1; // Fixed distance between states.
						
						d = distances.get(v);
						if ( d < 0 || alternative < d ) {
							distances.put(v, alternative);
							predecessors.put(v, u);
						}

					}
				}
			}
		}

		State u = to;
		
				
		while (predecessors.containsKey(u)) {
			if(u != from)
				p.addFirst(u);
			u = predecessors.get(u);
		}
		
		if(p.isEmpty())
			throw new IllegalStateException("Cannot get transition path from " + from + " to " + to + ".");

		predecessors.clear();
		distances.clear();
	
	}

	public void setTransitionHandler(TransitionHandler transitionHandler) {
		mTransitionHandler = transitionHandler;
		
	}
		
	@Override
	public void dispose() {
		finish();
	}

	@Override
	public void start() {
	}

	@Override
	public void finish() {
		
		while(!mPath.isEmpty())
			update();
		
		mCurrentState.finish();
	}

}
