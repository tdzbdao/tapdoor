package com.vectornoid.tapdoor.statemachine;

/**
 * 
 * Interface for handling state machine transitions.
 * 
 * @author void (tdzbdao@gmail.com)
 *
 */

public interface TransitionHandler {

	public boolean update(State oldState, State newState);
	public void start(State oldState, State newState);
}
