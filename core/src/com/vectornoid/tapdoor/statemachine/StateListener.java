package com.vectornoid.tapdoor.statemachine;

/**
 * 
 * @author void
 * 
 * Listener interface for state machine events.
 *
 */
public interface StateListener {

	/**
	 * Called when a state has been switched.
	 * @param from the old state
	 * @param to the new state.
	 */
	public void stateSwitched(State from, State to);
	
}
