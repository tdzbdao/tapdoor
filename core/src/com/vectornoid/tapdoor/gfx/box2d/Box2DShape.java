package com.vectornoid.tapdoor.gfx.box2d;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.VertexAttributes.Usage;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.Renderable;
import com.badlogic.gdx.graphics.g3d.RenderableProvider;
import com.badlogic.gdx.graphics.g3d.utils.MeshPartBuilder;
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.Shape;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Pool;
import com.vectornoid.tapdoor.system.Globals;

/**
 * A Box2DShape is a utility class for rendering Box2D shapes
 * inside the application.
 * 
 * This class is mainly for debugging and editing purposes, so there
 * is not much optimization to take place here.
 * 
 * @author void (tdzbdao@gmail.com)
 *
 */

public class Box2DShape implements RenderableProvider {

	private Shape mShape = null;
	private ModelInstance mModelInstance = null;

	public Box2DShape(Shape shape) {
		mShape = shape;

	}

	@Override
	public void getRenderables(Array<Renderable> renderables, Pool<Renderable> pool) {

		if(mShape != null && mModelInstance == null)
			initialize();
		if(mModelInstance != null)
			mModelInstance.getRenderables(renderables, pool);
	}

	/**
	 * Model creation function for circle shapes.
	 * @param shape
	 */
	private void createModel(CircleShape shape) {
		
		ModelBuilder mb = new ModelBuilder();

		mb.begin();
		MeshPartBuilder mpb = mb.part("shape", GL20.GL_LINES, Usage.Position | Usage.ColorPacked, new Material());
		Color col = Color.GREEN;
		float radius = Globals.boxToWorld(shape.getRadius());
		
		mpb.setColor(col);
		mpb.setVertexTransform(new Matrix4().idt().translate(Globals.boxToWorld(shape.getPosition().x), Globals.boxToWorld(shape.getPosition().y), 0));
		mpb.circle(radius, 8, 0, 0, 0, 0, 0, 1, 0, 360);
		
		mModelInstance = new ModelInstance(mb.end());
	}

	/**
	 * Model creation function for polygon shapes.
	 * @param shape
	 */
	private void createModel(PolygonShape shape) {
		int numVertices = shape.getVertexCount();

		Vector2 vertex = new Vector2();

		ModelBuilder mb = new ModelBuilder();

		mb.begin();
		MeshPartBuilder mpb = mb.part("shape", GL20.GL_LINES, Usage.Position | Usage.Color, new Material());
		
		Vector3 p1 = null, p2 = null, p0=null;
		Color col = Color.GREEN;
				
		for (int i = 0; i < numVertices; i++) {
			shape.getVertex(i, vertex);
			Globals.boxToWorld(vertex);

			p2 = new Vector3(vertex.x, vertex.y, 0);
			
			if(p1 != null)
				mpb.line(p1, col, p2, col);
			else
				p0 = p2;
			p1 = p2;
		}
		
		mpb.line(p0, col, p1, col);

		mModelInstance = new ModelInstance(mb.end());
	}
	
	private void initialize() {
		
		switch (mShape.getType()) {
		case Chain:
			break;
		case Circle:
			createModel((CircleShape) mShape);
			break;
		case Edge:
			// Edges are not implemented yet.
			break;
		case Polygon:
			createModel((PolygonShape) mShape);
			break;
		default:
			break;
		}
		
		//mShape.dispose();
		mShape = null;
	}


}
