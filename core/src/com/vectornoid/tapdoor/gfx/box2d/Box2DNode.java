package com.vectornoid.tapdoor.gfx.box2d;

import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.vectornoid.tapdoor.gfx.scene3d.SceneNode;
import com.vectornoid.tapdoor.system.Globals;

/**
 * A scene node that can be used to display a Box2D body. The node supports multiple fixtures per body, and will
 * try to consider local shape tranformations whenever possible.
 * 
 * @author void (tdzbdao@gmail.com)
 */

public class Box2DNode extends SceneNode {

	private Body mBody;

	final public static short COLLISION_FILTER_CATEGORY = 0x0001;

	public Box2DNode(SceneNode parent, Body body, boolean renderShapes) {
		super(parent);
		body.setUserData(this);

		setRenderBoundingBox(renderShapes);

		mBody = body;
	}

	public void setRenderShapes(boolean renderShapes) {

		removeAllRenderables();

		if (renderShapes)
			for (Fixture fix : mBody.getFixtureList()) {
				fix.getFilterData().categoryBits = COLLISION_FILTER_CATEGORY;
				if (renderShapes)
					attachRenderable(new Box2DShape(fix.getShape()));
			}
	}

	/**
	 * 
	 * @return The box2d body that is assigned to this node.
	 */
	public Body getBody() {
		return mBody;
	}

	@Override
	public void setPosition(Vector3 p) {

		mBody.setTransform(Globals.worldToBox(p.x), Globals.worldToBox(p.y), mBody.getAngle());
		super.setPosition(p);
	}
	
	@Override
	public void update() {
		updatePhysics();
		super.update();
	}

	public void updatePhysics() {

		if (mBody.getType() == BodyType.DynamicBody) {
			// If the body is dynamic, we let the physics simulation control its position.
			getPosition().add( // Interpolate the position, makes smoother transitions for low physics locksteps.
					(Globals.boxToWorld(mBody.getPosition().x) - getPosition().x) * 0.2f,
					(Globals.boxToWorld(mBody.getPosition().y) - getPosition().y) * 0.2f,
					0f
					);
			/*getPosition().x = Globals.boxToWorld(mBody.getPosition().x);
			getPosition().y = Globals.boxToWorld(mBody.getPosition().y);*/
		} else if (mBody.getType() == BodyType.KinematicBody) {
			// Kinematic bodies will be transformed according to the scene graph.
			mBody.setTransform(Globals.worldToBox(getPosition().x), Globals.worldToBox(getPosition().y), mBody.getAngle());
		}

		if (mBody.getType() != BodyType.StaticBody) // Only invalidate non-static bodies.
			invalidate(true, false);
	}

	@Override
	public void render(ModelBatch batch, Environment env) {

		// Apply the bodies angle (its z-transformation)
		if (mBody.getType() == BodyType.DynamicBody) {
			getOrientation().set(Vector3.Z, (float) (mBody.getAngle() * 180.0f / Math.PI));
		}

		super.render(batch, env);
	}

}
