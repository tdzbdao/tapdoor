attribute vec3 a_position;
attribute vec3 a_normal;
attribute vec2 a_texCoord0;
 
uniform mat4 u_worldTrans;
uniform mat4 u_projTrans;
uniform vec2 u_offset;
uniform vec2 u_dims;

varying vec2 v_texCoord0;
 
void main() {

	v_texCoord0 = a_texCoord0;

    gl_Position = u_projTrans * u_worldTrans * vec4( (a_position.x + u_offset.x) * u_dims.x, (a_position.y + u_offset.y) * u_dims.y, a_position.z, 1.0);
}