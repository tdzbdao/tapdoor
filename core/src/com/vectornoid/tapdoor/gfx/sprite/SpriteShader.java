package com.vectornoid.tapdoor.gfx.sprite;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g3d.Renderable;
import com.badlogic.gdx.graphics.g3d.Shader;
import com.badlogic.gdx.graphics.g3d.utils.RenderContext;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.vectornoid.tapdoor.gfx.sprite.Sprite.Flip;

/**
 * 
 * Shader class needed to render sprites.
 * 
 * @author void (tdzbdao@gmail.com)
 *
 */

public class SpriteShader implements Shader {

	private int u_projTrans;
	private int u_worldTrans;
	private int u_texCoords;
	private int u_texture;
	private int u_offset;
	private int u_dims;
	
	ShaderProgram program;
	
	public class Proxy implements Shader {
		private SpriteShader mOwner;
		private TextureRegion mRegion;

		public Proxy(SpriteShader owner, SpriteFrame frame) {
			mOwner = owner;
			mRegion = frame.getTextureRegion();
		}
		
		@Override
		public void begin(Camera camera, RenderContext context) {
			mOwner.begin(camera, context);
		}

		@Override
		public boolean canRender(Renderable instance) {
			return mOwner.canRender(instance);
		}

		@Override
		public int compareTo(Shader other) {
			return mOwner.compareTo(other);
		}

		@Override
		public void dispose() {
			// TODO Auto-generated method stub
			mOwner.dispose();
		}

		@Override
		public void end() {
			mOwner.end();
		}

		@Override
		public void init() {
			// TODO Auto-generated method stub
			mOwner.init();
		}

		@Override
		public void render(Renderable renderable) {
			// Pass our frame region to let the SpriteShader select UV and texture info.
			mOwner.render(mRegion, renderable);
		}
	}
	
	@Override
	public void dispose() {
		program.dispose();
	}

	@Override
	public void init() {
		
		String vert = Gdx.files.classpath("com/vectornoid/tapdoor/gfx/sprite/sprite.vertex.glsl").readString();
		String frag = Gdx.files.classpath("com/vectornoid/tapdoor/gfx/sprite/sprite.fragment.glsl").readString();

		program = new ShaderProgram(vert, frag);
		if (!program.isCompiled())
			throw new GdxRuntimeException(program.getLog());

		u_projTrans = program.getUniformLocation("u_projTrans");
		u_worldTrans = program.getUniformLocation("u_worldTrans");
		
		u_texCoords = program.getUniformLocation("u_texCoords");
		u_texture = program.getUniformLocation("u_texture");
		
		u_offset = program.getUniformLocation("u_offset");
		u_dims = program.getUniformLocation("u_dims");
	}

	@Override
	public int compareTo(Shader other) {
		return 0;
	}

	@Override
	public boolean canRender(Renderable instance) {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public void begin(Camera camera, RenderContext context) {
		
		program.begin();
		program.setUniformMatrix(u_projTrans, camera.combined);
		context.setDepthTest(GL20.GL_LEQUAL);
		context.setCullFace(GL20.GL_BACK);

	}

	@Override
	public void render(Renderable renderable) {
		
		Gdx.gl.glEnable(GL20.GL_BLEND); // Enable texture blending

		program.setUniformMatrix(u_worldTrans, renderable.worldTransform);
		renderable.mesh.render(program, renderable.primitiveType, renderable.meshPartOffset, renderable.meshPartSize);

	}
	
	public void render(TextureRegion mRegion, Renderable renderable) {
		
		mRegion.getTexture().bind(0);
		
		program.setUniformi(u_texture, 0);
		
		SpriteInstance instance = (SpriteInstance) renderable.userData;
		
		//program.setUniformMatrix(u_offsetMatrix, instance.mOffsetMatrix);
		program.setUniformf(u_offset, instance.getCurrentFrame().mOffset);
		program.setUniformf(u_dims, instance.getCurrentFrame().mDims);
				
		Flip flip = Flip.combine( instance.getFlip(), instance.getCurrentFrame().getFlip() );
						
		switch(flip) {
		
			case None: 
				program.setUniformf(u_texCoords, mRegion.getU(), mRegion.getV(), mRegion.getU2(), mRegion.getV2());
				break;
			case Horizontal:
				program.setUniformf(u_texCoords, mRegion.getU2(), mRegion.getV(), mRegion.getU(), mRegion.getV2());
				break;
			case Vertical:
				program.setUniformf(u_texCoords, mRegion.getU(), mRegion.getV2(), mRegion.getU2(), mRegion.getV());
				break;
			case Both:
				program.setUniformf(u_texCoords, mRegion.getU2(), mRegion.getV2(), mRegion.getU(), mRegion.getV());
				break;
				
		}
					
		render(renderable);
		
	}

	@Override
	public void end() {
		program.end();
	}
	
	public Proxy instance(SpriteFrame frame) {
		return new Proxy(this, frame);
	}


}
