package com.vectornoid.tapdoor.gfx.sprite;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.VertexAttributes.Usage;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.attributes.BlendingAttribute;
import com.badlogic.gdx.graphics.g3d.attributes.TextureAttribute;
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.Json.Serializable;
import com.badlogic.gdx.utils.JsonValue;
import com.badlogic.gdx.utils.ObjectMap;

public class Sprite implements Disposable, Serializable {

	public enum Flip {
		None, Horizontal, Vertical, Both;

		public static Flip combine(Flip a, Flip b) {

			switch (a) {
			case Horizontal:
				if (b == Flip.Horizontal)
					b = Flip.None;
				else if (b == Flip.Both)
					b = Flip.Vertical;
				else
					b = a;
				break;

			case Vertical:
				if (b == Flip.Vertical)
					b = Flip.None;
				else if (b == Flip.Both)
					b = Flip.Horizontal;
				else
					b = a;
				break;

			case Both:
				if (b == Flip.Horizontal)
					b = Flip.Vertical;
				else if (b == Flip.Vertical)
					b = Flip.Horizontal;
				else if (b == Flip.Both)
					b = Flip.None;
				else
					b = a;
				break;
			default:
				break;

			}

			return b;
		}

	};

	ObjectMap<String, Array<SpriteFrame>> mAnimations = new ObjectMap<>();
	Model mSpriteModel;
	public ModelInstance mModelInstance;
	Texture mTexture;
	SpriteShader mShader;

	public Sprite() {
	}

	public Sprite(Texture tex) {
		mTexture = tex;
		create();
	}

	private void create() {
		ModelBuilder mb = new ModelBuilder();
		
		float width = 1.0f;
		float height= 1.0f;
		
		mSpriteModel = mb.createRect(0, 0, 0, width, 0, 0,
				width, height, 0, 0, height, 0, 0, 0, 1,
				new Material(TextureAttribute.createDiffuse(mTexture),
						new BlendingAttribute(GL20.GL_SRC_ALPHA,
								GL20.GL_ONE_MINUS_SRC_ALPHA)), Usage.Position
						| Usage.Normal | Usage.TextureCoordinates);

		mModelInstance = new ModelInstance(mSpriteModel);

		mShader = new SpriteShader();
		mShader.init();
	}

	public void addFrame(int x, int y, int w, int h, float offsetx,
			float offsety, float duration, Flip flip) {
		addFrame(x, y, w, h, offsetx, offsety, flip, duration, "default");
	}

	public void addFrame(int x, int y, int w, int h, float offsetx,
			float offsety, Flip flip, float duration, String animation) {

		Array<SpriteFrame> frames;
		if (mAnimations.containsKey(animation))
			frames = mAnimations.get(animation);
		else {
			frames = new Array<SpriteFrame>();
			mAnimations.put(animation, frames);
		}

		frames.add(new SpriteFrame(this,
				new TextureRegion(mTexture, x, y, w, h), offsetx, offsety, flip, duration));
	}

	public void addFrames(int frameWidth, int frameHeight, int offset,
			int numFrames, float duration) {
		addFrames(frameWidth, frameHeight, offset, numFrames, duration, "default");
	}

	public void addFrames(int frameWidth, int frameHeight, int offset,
			int numFrames, float duration, String animation) {
		int framesPerRow = (int) Math.floor(mTexture.getWidth() / frameWidth);
		int x = offset % framesPerRow;
		int y = (int) Math.floor(offset / framesPerRow);

		for (int i = 0; i < numFrames; i++) {
			addFrame(x * frameWidth, y * frameHeight, frameWidth, frameHeight,
					0, 0, Flip.None, duration, animation);

			if (++x >= framesPerRow) {
				x = 0;
				if (++y * frameHeight >= mTexture.getHeight())
					break;
			}
		}
	}

	public SpriteInstance createInstance() {
		return new SpriteInstance(this);
	}

	@Override
	public void dispose() {

		mSpriteModel.dispose();

	}

	@Override
	public void write(Json json) {
		// Nah.
	}

	@Override
	public void read(Json json, JsonValue jsonData) {

		mTexture = new Texture(
				Gdx.files.internal(jsonData.getString("texture")));
		assert (mTexture != null);

		create();
		
		float defaultDuration = jsonData.getFloat("defaultDuration", 1.0f);

		for (JsonValue anim : jsonData.get("animations")) {
			
			for (JsonValue frame : anim) {
				if (!frame.isArray()) {
					try {
						int x = frame.getInt("x");
						int y = frame.getInt("y");
						int w = frame.getInt("w");
						int h = frame.getInt("h");
						float offsetX = frame.getFloat("offsetX",0.0f);
						float offsetY = frame.getFloat("offsetY",0.0f);
						float duration = frame.getFloat("duration", defaultDuration);
						Flip flip = Flip.valueOf(frame.getString("flip"));
						addFrame(x, y, w, h, offsetX, offsetY, flip, duration, anim.name);
					} catch (Exception e) {

					}
				}
			}
		}

	}
}
