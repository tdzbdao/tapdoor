package com.vectornoid.tapdoor.gfx.sprite;

import java.util.HashMap;

import com.badlogic.gdx.utils.Array;

/**
 * 
 * A sprite manager is capable of managing sprite instance updates over a period of time.
 * It is not required to have a sprite manager maintain sprite updates, but much more convenient than having to 
 * keep track of any instance at any time.
 * 
 * @author void (tdzbdao@gmail.com)
 *
 */

public class SpriteManager {

	private HashMap<String, Sprite> mSprites = new HashMap<String, Sprite>();
	private Array<SpriteInstance> mInstances = new Array<SpriteInstance>();
	
	public void update(float deltaTime) {
		for(SpriteInstance si : mInstances) {
			si.addTime(deltaTime);
		}
	}
	
	public void registerSprite(Sprite spr, String name) {
		mSprites.put(name,spr);
		
	}
	
	public SpriteInstance createInstance(String name) {
		
		
		SpriteInstance si = mSprites.get(name).createInstance();
		mInstances.add(si);
		return si;
	}
	
	public void removeInstance(SpriteInstance si) {
		mInstances.removeValue(si, true);
	}

	public Sprite get(String name) {
		return mSprites.get(name);
	}
	
}
