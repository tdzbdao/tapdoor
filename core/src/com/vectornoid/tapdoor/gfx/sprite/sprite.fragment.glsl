#ifdef GL_ES
precision mediump float;
#endif
 
uniform vec3 u_color;
uniform float u_alpha;

uniform sampler2D u_texture;
uniform vec4 u_texCoords;

varying vec2 v_texCoord0;
 
void main() {

	vec2 coords = vec2(u_texCoords.x + (u_texCoords.z - u_texCoords.x ) * v_texCoord0.x,
					   u_texCoords.y + (u_texCoords.w - u_texCoords.y ) * v_texCoord0.y
					   );
    gl_FragColor = texture2D(u_texture, coords);
    //gl_FragColor *= vec4(1,0,0,1);
}