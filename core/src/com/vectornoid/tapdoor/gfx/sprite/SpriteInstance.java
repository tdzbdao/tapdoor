package com.vectornoid.tapdoor.gfx.sprite;

import com.badlogic.gdx.graphics.g3d.Renderable;
import com.badlogic.gdx.graphics.g3d.RenderableProvider;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Pool;

/**
 * An actual renderable instance of a sprite.
 * 
 * @see Sprite.createInstance()
 * 
 * @author void (tdzbdao@gmail.com)
 *
 */

public class SpriteInstance implements RenderableProvider {
	
	float mDeltaTime = 0.0f;
	Sprite mSprite;
	float mTotalAnimationLength;
	int mCurrentFrame;
	private Sprite.Flip mFlip = Sprite.Flip.None;
	private boolean mLoop = true;
	private Array<SpriteFrame> mCurrentAnimation = null;

	SpriteInstance(Sprite src)
	{
		mSprite = src;
		mCurrentFrame = 0;
		mCurrentAnimation = mSprite.mAnimations.values().iterator().next();
	}
	
	public void setAnimation(String name) {
		mCurrentAnimation = mSprite.mAnimations.get(name);
	}
	
	public void setLoop(boolean loop) {
		mLoop = loop;
	}
	
	public boolean isDone() {
		return mCurrentFrame == mCurrentAnimation.size-1;
	}
	
	public void flip(Sprite.Flip f) {
		mFlip = f;
	}
	
	public Sprite.Flip getFlip() {
		return mFlip;
	}
	
	public Sprite getSprite() {
		return mSprite;
	}
	
	public void addTime(float delta)
	{
		if(delta < 0.0f)
			return;
		
		mDeltaTime += delta;
		
		SpriteFrame frame = mCurrentAnimation.get(mCurrentFrame);
		
		if(mDeltaTime <= frame.mDuration)
			return;
						
		while(mDeltaTime > frame.mDuration)
		{
			if(++mCurrentFrame >= mCurrentAnimation.size)
			{
				if(mLoop)  
					mCurrentFrame = 0;
				else {
					--mCurrentFrame;
					break;
				}
			}
			frame = mCurrentAnimation.get(mCurrentFrame);
			mDeltaTime -= frame.mDuration;
		}
	}
		
	public SpriteFrame getCurrentFrame()
	{
		return mCurrentAnimation.get(mCurrentFrame);
	}

	@Override
	public void getRenderables(Array<Renderable> renderables, Pool<Renderable> pool) {
						
		if(mCurrentAnimation.size == 0)
			return;
		
		SpriteFrame currentFrame = getCurrentFrame();
		
		Renderable renderable = mSprite.mModelInstance.getRenderable( pool.obtain() );
		renderable.shader = currentFrame.mShaderProxy;
		renderable.userData = this;
				
		renderables.add(renderable);
		
		//mSprite.mModelInstance.getRenderables(renderables, pool);
	}
}
