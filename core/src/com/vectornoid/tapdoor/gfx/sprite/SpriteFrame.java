package com.vectornoid.tapdoor.gfx.sprite;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.vectornoid.tapdoor.gfx.sprite.Sprite.Flip;

public class SpriteFrame {
	Vector2 mOffset = new Vector2();
	Vector2 mDims = new Vector2();
	Flip mFlip;
	Sprite mSprite;
	TextureRegion mTextureRegion;
	SpriteShader.Proxy mShaderProxy;
	float mDuration;

	SpriteFrame(Sprite spr, TextureRegion region, float offsetx, float offsety, Flip flip, float duration) {
		
		mOffset.set(offsetx,offsety);
		mDims.set(region.getRegionWidth(), region.getRegionHeight());
		mFlip = flip;
		mDuration = duration;

		mSprite = spr;
		mTextureRegion = region;
		
		mShaderProxy = spr.mShader.instance(this);
		
	}
	
	public Vector2 getOffset() {
		return mOffset;
	}

	public TextureRegion getTextureRegion() {
		return mTextureRegion;
	}

	public Flip getFlip() {
		return mFlip;
	}
	
	


}
