package com.vectornoid.tapdoor.gfx;

/**
 * An interface for renderables that support the frame rendering cycle.
 * 
 * A frame cycle consists of
 * 
 * - A preparation phase in which all objects get transformed/modified to be ready to get rendered, 
 * - A render phase in which the prepared objects will be rendered 
 * - An end phase which may perform some cleanup.
 * 
 * When using frame buffers, the same frame might be rendered multiple times (as needed for deferred shading, for example). 
 * Therefore, it is important that the scene will not be modified inside the render phase.
 * 
 * @author void (tdzbdao@gmail.com)
 * 
 */
public interface FrameRenderable {

	public void prepareFrame();

	public void renderFrame();

	public void endFrame();

}
