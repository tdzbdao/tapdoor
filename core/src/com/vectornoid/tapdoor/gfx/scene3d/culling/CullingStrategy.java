package com.vectornoid.tapdoor.gfx.scene3d.culling;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.utils.Array;
import com.vectornoid.tapdoor.gfx.scene3d.SceneNode;

/**
 * A custom CullingStrategy can be implemented to optimize the scene rendering
 * towards more specialized situations.
 * 
 * By default, the Scene class uses a TreeCullingStrategy, which traverses the scene nodes
 * based upon their hierarchy and performs bounding box tests with the camera frustum.
 * 
 * @author void (tdzbdao@gmail.com)
 *
 */
public interface CullingStrategy {

	/**
	 * Called each frame, returns a list of visible nodes.
	 * @param cam The current camera used for rendering.
	 * @param root The root scene node (not necessarily the scenes absolute root).
	 * @param nodes An array of nodes to render.
	 */
	public void getVisibleNodes(Camera cam, SceneNode root, Array<SceneNode> nodes);

}
