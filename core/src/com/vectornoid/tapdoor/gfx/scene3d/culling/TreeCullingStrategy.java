package com.vectornoid.tapdoor.gfx.scene3d.culling;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.utils.Array;
import com.vectornoid.tapdoor.gfx.scene3d.SceneNode;

/**
 * This is the default scene culling strategy. It recursively iterates over the scene graph and checks for the nodes bounding boxes.
 * In order to estimate visible scene nodes, each node must have a local bounding box with non-zero width/length/height.
 * 
 * @author void (tdzbdao@gmail.com)
 *
 */

public class TreeCullingStrategy implements CullingStrategy {

	@Override
	public void getVisibleNodes(Camera cam, SceneNode root, Array<SceneNode> nodes) {
		
		root.update();
		
		if(!root.isVisible())
			return;
								
		if( cam.frustum.boundsInFrustum(root.getWorldBounds()) )
		{	
			if(root.getRenderables().size > 0 || root.getRenderBoundingBox())
			{
				nodes.add(root);
			}
				
			for(int i=0;i<root.getChildren().size;i++) {
				SceneNode n = root.getChildren().get(i);
				getVisibleNodes(cam, n, nodes);
			}
				
		}
	}

}
