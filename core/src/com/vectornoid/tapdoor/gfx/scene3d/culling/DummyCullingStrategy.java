package com.vectornoid.tapdoor.gfx.scene3d.culling;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.utils.Array;
import com.vectornoid.tapdoor.gfx.scene3d.SceneNode;

/**
 * A dummy culling strategy won't cull anything.
 * 
 * @author void (tdzbdao@gmail.com)
 * 
 */
public class DummyCullingStrategy implements CullingStrategy {

	@Override
	public void getVisibleNodes(Camera cam, SceneNode root, Array<SceneNode> nodes) {

		if (root.isVisible()) {
			nodes.add(root);

			for (int i = 0; i < root.getChildren().size; i++) {
				getVisibleNodes(cam, root.getChildren().get(i), nodes);
			}
		}

	}

}
