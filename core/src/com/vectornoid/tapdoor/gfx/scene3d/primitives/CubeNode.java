package com.vectornoid.tapdoor.gfx.scene3d.primitives;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.VertexAttributes.Usage;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder;
import com.badlogic.gdx.math.collision.BoundingBox;
import com.vectornoid.tapdoor.gfx.scene3d.SceneNode;
import com.vectornoid.tapdoor.gfx.scene3d.collision.BoxCollider;

public class CubeNode extends SceneNode {

	private /*static*/ Model mCubeModel = null; 
	private ModelInstance mModelInstance;
	
	public CubeNode(SceneNode parent, String name, Material mat) {
		super(parent);
		build(new Material[]{mat});
	}
	
	public CubeNode(SceneNode parent, String name, Material[] mats) {
		super(parent);
		build(mats);
	}
	
	public CubeNode(SceneNode parent, String name) {
		super(parent, name);
		build(null);
	}
	
	public CubeNode(SceneNode parent) {
		super(parent);
		build(null);
	}
			
	private void build(Material[] material) {
		
		if(material == null)
			material = new Material[] { new Material(ColorAttribute.createDiffuse(Color.WHITE)) };
		
		if(mCubeModel == null) {
			ModelBuilder mb = new ModelBuilder();
			mCubeModel = mb.createBox(1, 1, 1, material[0], Usage.Normal | Usage.TextureCoordinates | Usage.Position);
		}
		
		mModelInstance = new ModelInstance(mCubeModel);
		mModelInstance.materials.clear();
		mModelInstance.materials.add(material[0]);
				
		attachRenderable(mModelInstance);
		
		setLocalBounds(mModelInstance.calculateBoundingBox(new BoundingBox()));
		setCollider(new BoxCollider(getLocalBounds()));
	}
	
	public ModelInstance getModelInstance() {
		return mModelInstance;
	}
	
	
}
