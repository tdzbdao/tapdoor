package com.vectornoid.tapdoor.gfx.scene3d.primitives;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.VertexAttributes.Usage;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.utils.MeshPartBuilder;
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder;
import com.badlogic.gdx.math.collision.BoundingBox;
import com.vectornoid.tapdoor.gfx.scene3d.SceneNode;
import com.vectornoid.tapdoor.gfx.scene3d.collision.BoxCollider;

public class PlaneNode extends SceneNode {

	private static Model mPlaneModel = null; 
	private ModelInstance mModelInstance;
	
	public PlaneNode(SceneNode parent, String name) {
		super(parent, name);
		build();
	}
	
	public PlaneNode(SceneNode parent) {
		super(parent);
		build();
	}
		
	private void build() {
		
		Material material = new Material(ColorAttribute.createDiffuse(Color.WHITE));
		
		if(mPlaneModel == null) {
			ModelBuilder mb = new ModelBuilder();
			mb.begin();
			MeshPartBuilder mpb = mb.part("Plane", GL20.GL_TRIANGLES, Usage.Normal | Usage.TextureCoordinates | Usage.Position, material);
			mpb.patch( 0,0,0,1,0,0,1,1,0,0,1,0, 0,0,-1, 1, 1);
			mPlaneModel = mb.end();
		}
		
		mModelInstance = new ModelInstance(mPlaneModel);
		//mModelInstance.materials.clear();
		//mModelInstance.materials.add(material);
		
		attachRenderable(mModelInstance);
		
		setLocalBounds(mModelInstance.calculateBoundingBox(new BoundingBox()).ext(0,0,0.1f)); // Extend the z-value of the bounding box to a non-zero value.
		setCollider(new BoxCollider(getLocalBounds()));
	}
	
	public ModelInstance getModelInstance() {
		return mModelInstance;
	}
	
 
	
	
}
