package com.vectornoid.tapdoor.gfx.scene3d;

public interface SceneListener {

	/**
	 * Fired when a bounding box has been modified (this includes modifications of the nodes world transformations).
	 * @param n The node that triggered this event.
	 */
	public void nodeBoundsChanged(SceneNode n);
	
	/**
	 * Fired when a node has been inserted in the scene graph.
	 * @param n The node that triggered this event.
	 */
	public void nodeInserted(SceneNode n);
	
	/**
	 * Fired when a nodes local transformation has been modified.
	 * @param n The node that triggered this event.
	 */
	public void nodeMoved(SceneNode n);
	
	/**
	 * Fired when a node has been removed from the scene graph.
	 * @param n The node that triggered this event.
	 */
	public void nodeRemoved(SceneNode n);
	
	/**
	 * Fired when a nodes visibility flag has been altered. 
	 * @param n The node that triggered this event.
	 */
	public void nodeVisibilityChanged(SceneNode n);
	
}
