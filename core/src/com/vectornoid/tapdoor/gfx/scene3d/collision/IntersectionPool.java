package com.vectornoid.tapdoor.gfx.scene3d.collision;

import com.badlogic.gdx.utils.Pool;

public class IntersectionPool extends Pool<SceneNodeIntersection> {
	@Override
	protected SceneNodeIntersection newObject() {
		return new SceneNodeIntersection();
	}
}
