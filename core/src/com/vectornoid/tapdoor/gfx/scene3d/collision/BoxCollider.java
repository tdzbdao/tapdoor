package com.vectornoid.tapdoor.gfx.scene3d.collision;

import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.math.collision.BoundingBox;
import com.badlogic.gdx.math.collision.Ray;
import com.badlogic.gdx.utils.Pool;
import com.vectornoid.tapdoor.gfx.scene3d.SceneNode;

public class BoxCollider extends SceneNodeCollider {

	private Vector3 mHitPoint = new Vector3();
	private Matrix4 mTransformation = new Matrix4();
	private float[] mSrcTriangles, mTriangles;
	private boolean mNeedsUpdate = false;
	//private BoundingBox mBoundingBox, mSourceBoundingBox;
	
	public BoxCollider(BoundingBox bb) {
		mSrcTriangles = new float[3 * 3 * 6 * 2];
		
		//mSourceBoundingBox = bb;
		//mBoundingBox = new BoundingBox(bb);
		updateBoundingBox(bb);
		mTriangles = mSrcTriangles.clone();
	}

	public void updateBoundingBox(BoundingBox bb) {

		int i = 0;

		// Upper side
		mSrcTriangles[i++] = bb.min.x;
		mSrcTriangles[i++] = bb.min.y;
		mSrcTriangles[i++] = bb.min.z;

		mSrcTriangles[i++] = bb.max.x;
		mSrcTriangles[i++] = bb.min.y;
		mSrcTriangles[i++] = bb.min.z;

		mSrcTriangles[i++] = bb.min.x;
		mSrcTriangles[i++] = bb.min.y;
		mSrcTriangles[i++] = bb.max.z;

		mSrcTriangles[i++] = bb.max.x;
		mSrcTriangles[i++] = bb.min.y;
		mSrcTriangles[i++] = bb.min.z;

		mSrcTriangles[i++] = bb.max.x;
		mSrcTriangles[i++] = bb.min.y;
		mSrcTriangles[i++] = bb.max.z;

		mSrcTriangles[i++] = bb.min.x;
		mSrcTriangles[i++] = bb.min.y;
		mSrcTriangles[i++] = bb.max.z;

		// Lower side
		mSrcTriangles[i++] = bb.min.x;
		mSrcTriangles[i++] = bb.max.y;
		mSrcTriangles[i++] = bb.min.z;

		mSrcTriangles[i++] = bb.max.x;
		mSrcTriangles[i++] = bb.max.y;
		mSrcTriangles[i++] = bb.min.z;

		mSrcTriangles[i++] = bb.min.x;
		mSrcTriangles[i++] = bb.max.y;
		mSrcTriangles[i++] = bb.max.z;

		mSrcTriangles[i++] = bb.max.x;
		mSrcTriangles[i++] = bb.max.y;
		mSrcTriangles[i++] = bb.min.z;

		mSrcTriangles[i++] = bb.max.x;
		mSrcTriangles[i++] = bb.max.y;
		mSrcTriangles[i++] = bb.max.z;

		mSrcTriangles[i++] = bb.min.x;
		mSrcTriangles[i++] = bb.max.y;
		mSrcTriangles[i++] = bb.max.z;

		// Back side
		mSrcTriangles[i++] = bb.min.x;
		mSrcTriangles[i++] = bb.min.y;
		mSrcTriangles[i++] = bb.min.z;

		mSrcTriangles[i++] = bb.max.x;
		mSrcTriangles[i++] = bb.min.y;
		mSrcTriangles[i++] = bb.min.z;

		mSrcTriangles[i++] = bb.max.x;
		mSrcTriangles[i++] = bb.max.y;
		mSrcTriangles[i++] = bb.min.z;

		mSrcTriangles[i++] = bb.min.x;
		mSrcTriangles[i++] = bb.min.y;
		mSrcTriangles[i++] = bb.min.z;

		mSrcTriangles[i++] = bb.max.x;
		mSrcTriangles[i++] = bb.max.y;
		mSrcTriangles[i++] = bb.min.z;

		mSrcTriangles[i++] = bb.min.x;
		mSrcTriangles[i++] = bb.max.y;
		mSrcTriangles[i++] = bb.min.z;

		// Front side
		mSrcTriangles[i++] = bb.min.x;
		mSrcTriangles[i++] = bb.min.y;
		mSrcTriangles[i++] = bb.max.z;

		mSrcTriangles[i++] = bb.max.x;
		mSrcTriangles[i++] = bb.min.y;
		mSrcTriangles[i++] = bb.max.z;

		mSrcTriangles[i++] = bb.max.x;
		mSrcTriangles[i++] = bb.max.y;
		mSrcTriangles[i++] = bb.max.z;

		mSrcTriangles[i++] = bb.min.x;
		mSrcTriangles[i++] = bb.min.y;
		mSrcTriangles[i++] = bb.max.z;

		mSrcTriangles[i++] = bb.max.x;
		mSrcTriangles[i++] = bb.max.y;
		mSrcTriangles[i++] = bb.max.z;

		mSrcTriangles[i++] = bb.min.x;
		mSrcTriangles[i++] = bb.max.y;
		mSrcTriangles[i++] = bb.max.z;

		// Left
		mSrcTriangles[i++] = bb.min.x;
		mSrcTriangles[i++] = bb.min.y;
		mSrcTriangles[i++] = bb.min.z;

		mSrcTriangles[i++] = bb.min.x;
		mSrcTriangles[i++] = bb.max.y;
		mSrcTriangles[i++] = bb.min.z;

		mSrcTriangles[i++] = bb.min.x;
		mSrcTriangles[i++] = bb.max.y;
		mSrcTriangles[i++] = bb.max.z;

		mSrcTriangles[i++] = bb.min.x;
		mSrcTriangles[i++] = bb.max.y;
		mSrcTriangles[i++] = bb.min.z;

		mSrcTriangles[i++] = bb.min.x;
		mSrcTriangles[i++] = bb.max.y;
		mSrcTriangles[i++] = bb.max.z;

		mSrcTriangles[i++] = bb.min.x;
		mSrcTriangles[i++] = bb.min.y;
		mSrcTriangles[i++] = bb.max.z;

		// Right
		mSrcTriangles[i++] = bb.max.x;
		mSrcTriangles[i++] = bb.min.y;
		mSrcTriangles[i++] = bb.min.z;

		mSrcTriangles[i++] = bb.max.x;
		mSrcTriangles[i++] = bb.max.y;
		mSrcTriangles[i++] = bb.min.z;

		mSrcTriangles[i++] = bb.max.x;
		mSrcTriangles[i++] = bb.max.y;
		mSrcTriangles[i++] = bb.max.z;

		mSrcTriangles[i++] = bb.max.x;
		mSrcTriangles[i++] = bb.max.y;
		mSrcTriangles[i++] = bb.min.z;

		mSrcTriangles[i++] = bb.max.x;
		mSrcTriangles[i++] = bb.max.y;
		mSrcTriangles[i++] = bb.max.z;

		mSrcTriangles[i++] = bb.max.x;
		mSrcTriangles[i++] = bb.min.y;
		mSrcTriangles[i++] = bb.max.z;
		
	}

	private void updateTransform() {
		
		//mBoundingBox.set(mSourceBoundingBox).mul(mTransformation);
		//updateBoundingBox(mBoundingBox);
				
		for (int i = 0; i < mSrcTriangles.length; i += 3) {
			mTriangles[i + 0] = mSrcTriangles[i + 0] * mTransformation.val[Matrix4.M00] + mSrcTriangles[i + 1] * mTransformation.val[Matrix4.M01] + mSrcTriangles[i + 2] * mTransformation.val[Matrix4.M02] + mTransformation.val[Matrix4.M03];
			mTriangles[i + 1] = mSrcTriangles[i + 0] * mTransformation.val[Matrix4.M10] + mSrcTriangles[i + 1] * mTransformation.val[Matrix4.M11] + mSrcTriangles[i + 2] * mTransformation.val[Matrix4.M12] + mTransformation.val[Matrix4.M13];
			mTriangles[i + 2] = mSrcTriangles[i + 0] * mTransformation.val[Matrix4.M20] + mSrcTriangles[i + 1] * mTransformation.val[Matrix4.M21] + mSrcTriangles[i + 2] * mTransformation.val[Matrix4.M22] + mTransformation.val[Matrix4.M23];
		}
		
	}

	@Override
	public SceneNodeIntersection rayQuery(SceneNode node, Ray ray, Pool<SceneNodeIntersection> pool) {

		if (mNeedsUpdate) {
			updateTransform();
		}

		if (Intersector.intersectRayTriangles(ray, mTriangles, mHitPoint)) {
			SceneNodeIntersection scn = pool.obtain();
			scn.mHitpoint.set(mHitPoint);
			scn.mNode = node;
			return scn;
		}

		return null;
	}

	@Override
	public SceneNodeIntersection pointQuery(SceneNode node, Vector3 pos, Pool<SceneNodeIntersection> pool) {

		if (mNeedsUpdate)
			updateTransform();

		return null;
	}

	@Override
	public void updateTransform(Matrix4 transform) {
				
		mTransformation.set(transform);
		mNeedsUpdate = true;
	}

}
