package com.vectornoid.tapdoor.gfx.scene3d.collision;

import java.util.Comparator;

import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.math.collision.Ray;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Pool;
import com.vectornoid.tapdoor.gfx.scene3d.SceneNode;

/**
 * A helper class for traversing a SceneNode hierarchy and
 * query for various intersection types (ray, point).
 * 
 * This class is not thread safe!
 * 
 * @author void (tdzbdao@gmail.com)
 *
 */
public class SceneIntersector {
		
	private static class SceneNodeIntersectionZComparator implements Comparator<SceneNodeIntersection>
	{
		Ray ray = null;
		
		@Override
		public int compare(SceneNodeIntersection o1, SceneNodeIntersection o2) {
			if(o1.mHitpoint.dst2(ray.origin) < o2.mHitpoint.dst2(ray.origin))
				return -1;
			return 1;
		}
		
	}
	
	private static SceneNodeIntersectionZComparator mComparator = new SceneNodeIntersectionZComparator();
	
	/**
	 * Performs a ray intersection query.
	 * @param root
	 * @param ray
	 * @param zSort
	 * @return
	 */
	public static Array<SceneNodeIntersection> query(SceneNode root, Ray ray, boolean zSort, Array<SceneNodeIntersection> result, Pool<SceneNodeIntersection> pool, long mask)
	{
		//Array<SceneNodeIntersection> result = new Array<SceneNodeIntersection>();
		result.clear();
		
		query(result, root, ray, pool, mask);
		
		if(zSort)
		{
			mComparator.ray = ray;
			result.sort(mComparator);
		}
		
		return result;
		
	}
	
	/**
	 * Performs a point test query.
	 * @param root
	 * @param pos
	 * @return
	 */
	
	public static Array<SceneNodeIntersection> query(SceneNode root, Vector3 pos, Array<SceneNodeIntersection> result, Pool<SceneNodeIntersection> pool, long mask)
	{		
		query(result, root, pos, pool, mask);
		
		return result;
	}
		
	private static void query(Array<SceneNodeIntersection> result, SceneNode root, Ray ray, Pool<SceneNodeIntersection> pool, long mask)
	{	
		if(!Intersector.intersectRayBoundsFast(ray, root.getWorldBounds()))
			return; 
						
		Array<SceneNode> children = root.getChildren();
		
		for(int i=0;i<children.size;i++) {
			SceneNode n = children.get(i);
			query(result, n, ray, pool, mask);
		}
						
		if(root.getCollider() != null && (root.getCollider().getCollisionMask() & mask) > 0 ) { // Fine grained scene node collider collision
			SceneNodeIntersection sni = root.getCollider().rayQuery(root, ray, pool);
			if(sni != null)
				result.add(sni);
		}
	}
	
	private static void query(Array<SceneNodeIntersection> result, SceneNode root, Vector3 pos, Pool<SceneNodeIntersection> pool, long mask)
	{
		if(!root.getWorldBounds().contains(pos))
			return;
				
		for(SceneNode n : root.getChildren())
		{
			query(result, n, pos, pool, mask);
		}
		
		if(root.getCollider() != null && (root.getCollider().getCollisionMask() & mask) > 0 ) { // Fine grained scene node collider collision
			SceneNodeIntersection sni = root.getCollider().pointQuery(root, pos, pool);
			if(sni != null)
				result.add(sni);
		}
	}
	
}
