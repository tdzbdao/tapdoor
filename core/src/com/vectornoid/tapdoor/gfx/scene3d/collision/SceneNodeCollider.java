package com.vectornoid.tapdoor.gfx.scene3d.collision;

import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.math.collision.Ray;
import com.badlogic.gdx.utils.Pool;
import com.vectornoid.tapdoor.gfx.scene3d.SceneNode;

/**
 * 
 * Interface for customizable per-node collision hull queries.  
 * 
 * @author void
 *
 */

public abstract class SceneNodeCollider {

	private long mCollisionMask = 0x0;
	
	public abstract void updateTransform(Matrix4 transform); 
	
	public abstract SceneNodeIntersection rayQuery(SceneNode node, Ray ray, Pool<SceneNodeIntersection> pool);
	public abstract SceneNodeIntersection pointQuery(SceneNode node, Vector3 pos, Pool<SceneNodeIntersection> pool);
	
	public void setCollisionMask(long mask) {
		mCollisionMask = mask;
	}
	
	public void addCollisionFlag(long flag) {
		mCollisionMask |= flag;
	}
	
	public void removeCollisionFlag(long flag) {
		mCollisionMask &= ~flag;
	}

	public long getCollisionMask() {
		return mCollisionMask;
	}
	
}
