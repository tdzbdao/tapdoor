package com.vectornoid.tapdoor.gfx.scene3d.collision;

import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Pool.Poolable;
import com.vectornoid.tapdoor.gfx.scene3d.SceneNode;

public class SceneNodeIntersection implements Poolable
{
	SceneNode mNode;
	Vector3 mHitpoint = new Vector3();
	
	public void init(SceneNode node, Vector3 hitpoint) {
		mNode = node;
		mHitpoint = hitpoint;
	}
	
	public Vector3 getHitpoint()
	{
		return mHitpoint;
	}
		
	public SceneNode getSceneNode()
	{
		return mNode;
	}

	@Override
	public void reset() {
		mNode = null;
		mHitpoint = null;
		
	}
	
	@Override
	public String toString() {
		return mNode.toString() + "@" + mHitpoint; 

	}
}
