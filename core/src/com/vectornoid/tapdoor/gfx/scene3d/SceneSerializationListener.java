package com.vectornoid.tapdoor.gfx.scene3d;

import com.vectornoid.tapdoor.system.SerializedReader;
import com.vectornoid.tapdoor.system.SerializedWriter;

public interface SceneSerializationListener {

	public void serializeNode(SerializedWriter writer, SceneNode node);	
	public SceneNode unserializeNode(SerializedReader reader, SceneNode parent);
	
}
