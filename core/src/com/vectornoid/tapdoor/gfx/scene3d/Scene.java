package com.vectornoid.tapdoor.gfx.scene3d;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Queue;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.Renderable;
import com.badlogic.gdx.graphics.g3d.RenderableProvider;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.utils.Array;
import com.vectornoid.tapdoor.gfx.FrameRenderable;
import com.vectornoid.tapdoor.gfx.scene3d.culling.CullingStrategy;
import com.vectornoid.tapdoor.gfx.scene3d.culling.TreeCullingStrategy;

/**
 * A scene contains a set of SceneNodes for rendering. While the SceneNodes are arranged in a tree layout, 
 * an additional ScenePartitionStrategy may be applied to the scene.
 * 
 * @author void (tdzbdao@gmail.com)
 * 
 */
public class Scene implements FrameRenderable {

	/**
	 * TransformedModelBatch allows modifications in the renderables world transformations on a per-frame basis.
	 */
	static protected class TransformedModelBatch extends ModelBatch {

		public void render(final RenderableProvider renderableProvider, final Environment environment, final Matrix4 transform) {
			final int offset = renderables.size;
			renderableProvider.getRenderables(renderables, renderablesPool);
			for (int i = offset; i < renderables.size; i++) {
				Renderable renderable = renderables.get(i);
				renderable.environment = environment;
				renderable.worldTransform.set(transform);
				if(renderable.shader == null)
					renderable.shader = shaderProvider.getShader(renderable);
			}
		}
	}

	/**
	 * 
	 * A node operation is a queued modification of the scene graph that has been deferred to the rendering loop.
	 * 
	 */
	static class NodeOperation {
		SceneNode node;
		NodeOperationType type;

		public NodeOperation(SceneNode n, NodeOperationType t) {
			node = n;
			type = t;
		}
	}

	/**
	 * 
	 *  Deferred node operations
	 *   
	 */
	static enum NodeOperationType {
		Insert, Remove, VisibilityChanged
	}

	protected Environment mEnvironment;
	protected Camera mCamera;

	private SceneNode mRootSceneNode;

	TransformedModelBatch mSceneNodeBatch;
	private Array<SceneNode> mRenderNodeCache = new Array<SceneNode>();

	Queue<NodeOperation> mQueuedNodeOperations = new LinkedList<NodeOperation>();
	private Array<SceneListener> mListeners = new Array<SceneListener>();
	SceneSerializationListener mSerializationListener = null;

	CullingStrategy mCullingStrategy = new TreeCullingStrategy();

	HashMap<String, SceneNode> mNodeLookup = new HashMap<String, SceneNode>();
	
	private static long mNextNodeID=1;
	
	long getUniqueNodeID() {
		return mNextNodeID++;
	}

	public Scene() {
		initialize(new PerspectiveCamera(40.0f, Gdx.graphics.getWidth(), Gdx.graphics.getHeight()));
	}

	public Scene(Camera camera) {
		initialize(camera);
	}

	public void dispose() {
		mRootSceneNode.dispose();
		mSceneNodeBatch.dispose();
	}

	public void setCamera(Camera camera) {
		mCamera = camera;
	}

	public Camera getCamera() {
		return mCamera;
	}

	public Environment getEnvironment() {
		return mEnvironment;
	}

	public SceneNode getRootSceneNode() {
		return mRootSceneNode;
	}

	public void addSceneListener(SceneListener l) {
		mListeners.add(l);
	}

	public void removeSceneListener(SceneListener l) {
		mListeners.removeValue(l, true);
	}

	public SceneNode getSceneNode(String name) {
		return mNodeLookup.get(name);
	}

	/**
	 * Updates the SceneNode tree.
	 */
	@Override
	public void prepareFrame() {
		processNodeOperations();
		mCamera.update();

		mCullingStrategy.getVisibleNodes(mCamera, mRootSceneNode, mRenderNodeCache);
	}

	@Override
	public void endFrame() {
		mRenderNodeCache.clear();
	}

	public int getVisibleNodeCount() {
		return mRenderNodeCache.size;
	}

	/**
	 * Renders the scene. Needs to be called after prepareFrame().
	 * 
	 */
	public void renderFrame() {

		// Gdx.gl20.glClearColor(0.8f, 0.8f, 0.8f, 1f);
		// Gdx.gl20.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
		Gdx.gl20.glEnable(GL20.GL_BLEND);
		Gdx.gl20.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
		Gdx.gl20.glBlendEquation(GL20.GL_BLEND);
		Gdx.gl20.glEnable(GL20.GL_TEXTURE_2D);	

		mSceneNodeBatch.begin(mCamera);

		for (int i = 0; i < mRenderNodeCache.size; i++) {
			SceneNode n = mRenderNodeCache.get(i);
			n.render(mSceneNodeBatch, mEnvironment);
		}
		
		mSceneNodeBatch.end();
	}

	final public void setCullingStrategy(CullingStrategy s) {
		mCullingStrategy = s;
	}

	/**
	 * Updates the viewport dimensions. Uses Gdx.graphics.getWidth/Height() to determine the viewport size.
	 */
	public void setupViewport() {
		mCamera.viewportWidth = Gdx.graphics.getWidth();
		mCamera.viewportHeight = Gdx.graphics.getHeight();
	}

	/**
	 * Updates the viewport dimensions.
	 */
	public void setupViewport(float w, float h) {
		mCamera.viewportWidth = w;
		mCamera.viewportHeight = h;
	}

	private void initialize(Camera cam) {

		mEnvironment = new Environment();
		mEnvironment.set(new ColorAttribute(ColorAttribute.AmbientLight, 0.4f, 0.4f, 0.4f, 1f));

		mCamera = cam;

		mRootSceneNode = new SceneNode(null, "SceneRoot");
		mRootSceneNode.mID = getUniqueNodeID();
		mRootSceneNode.mScene = this;

		mSceneNodeBatch = new TransformedModelBatch();
	}

	private void processNodeOperations() {
		
		NodeOperation op;
		
		while ((op = mQueuedNodeOperations.poll()) != null) {

			op.node.update();

			for (int i = 0; i < mListeners.size; i++) {

				SceneListener l = mListeners.get(i);

				switch (op.type) {
				case Remove:
					l.nodeRemoved(op.node);
					op.node.invalidate(true, true);
					break;
				case Insert:
					op.node.init();
					l.nodeInserted(op.node);
					op.node.invalidate(true, true);
					break;
				case VisibilityChanged:
					l.nodeVisibilityChanged(op.node);
					op.node.invalidate(false, true);
					break;

				}

			}
		}
	}

	public CullingStrategy getCullingStrategy() {
		return mCullingStrategy;
	}

}
