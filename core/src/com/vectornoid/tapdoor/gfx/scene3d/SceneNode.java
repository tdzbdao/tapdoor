package com.vectornoid.tapdoor.gfx.scene3d;

import java.util.HashMap;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.VertexAttributes.Usage;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.RenderableProvider;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.utils.MeshPartBuilder;
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Quaternion;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.math.collision.BoundingBox;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Disposable;
import com.vectornoid.tapdoor.gfx.scene3d.Scene.NodeOperation;
import com.vectornoid.tapdoor.gfx.scene3d.Scene.NodeOperationType;
import com.vectornoid.tapdoor.gfx.scene3d.Scene.TransformedModelBatch;
import com.vectornoid.tapdoor.gfx.scene3d.collision.SceneNodeCollider;
import com.vectornoid.tapdoor.system.Serializable;
import com.vectornoid.tapdoor.system.SerializedReader;
import com.vectornoid.tapdoor.system.SerializedWriter;

/**
 * A SceneNode is an object aligned within a Scene that can hold renderable objects attached to it. To properly perform culling tests and scene queries, each SceneNode should provide a bounding box defining its virtual scene dimensions.
 * 
 * @author void (tdzbdao@gmail.com)
 */
public class SceneNode implements Serializable, Disposable {

	Vector3 mPosition = new Vector3(0, 0, 0);
	Quaternion mOrientation = new Quaternion();
	Vector3 mScale = new Vector3(1, 1, 1);
	Matrix4 mLocalTransform = new Matrix4().idt();
	Matrix4 mWorldTransform = new Matrix4().idt();

	BoundingBox mLocalBounds = new BoundingBox().inf();
	BoundingBox mWorldBounds = new BoundingBox();

	Scene mScene = null;

	private boolean mUpdateTransform = true;
	private boolean mUpdateBoundingBox = true;
	private boolean mVisible = true;

	private SceneNode mParent;
	private Array<SceneNode> mChildren = new Array<SceneNode>();
	Array<RenderableProvider> mRenderables = new Array<RenderableProvider>();
	Model mDebugBoundsModel = null;
	Color mDebugColor = new Color((float) Math.random(), (float) Math.random(), (float) Math.random(), 1.0f);
	ModelInstance mDebugBoundsModelInstance = null;
	boolean mRenderDebugBounds = false;

	private String mName = "";
	private HashMap<String, Object> mProperties = new HashMap<>();
	private SceneNodeCollider mCollider = null;
	long mID;
	private Quaternion mTmpQuat = new Quaternion();
	private Vector2 mTmpVector = new Vector3();

	/**
	 * Constructor.
	 * 
	 * @param parent
	 *            The parent scene node.
	 * @param name
	 *            The unique scene node identifier. May be NULL.
	 */
	public SceneNode(SceneNode parent, String name) {

		if (parent != null && parent.getScene() != null)
			mID = parent.getScene().getUniqueNodeID();

		mName = (name != null ? name : "");
		mParent = parent;

		if (parent != null) {
			mScene = parent.mScene;
			parent.mChildren.add(this);
			if (mScene != null)
				mScene.mQueuedNodeOperations.add(new NodeOperation(this, Scene.NodeOperationType.Insert));
		}

		if (name != null) {
			if (mScene != null) {
				if (mScene.mNodeLookup.containsKey(name))
					throw (new IllegalArgumentException("A scene node with the name " + name + " already exists."));
				mScene.mNodeLookup.put(name, this);
			}
		}
	}

	public long getID() {
		return mID;
	}

	public SceneNodeCollider getCollider() {
		return mCollider;
	}

	public void setCollider(SceneNodeCollider c) {
		mCollider = c;
	}

	public void setDebugColor(Color col) {
		mDebugColor = col;
	}

	/**
	 * Constructor.
	 * 
	 * @param parent
	 *            The parent scene node.
	 */
	public SceneNode(SceneNode parent) {
		this(parent, null);
	}

	public void setParent(SceneNode parent) {
		if (mParent == parent)
			return;

		if (mParent != null) {
			remove();
		}

		if (parent != null) {
			mScene = parent.mScene;
			parent.mChildren.add(this);
			mScene.mQueuedNodeOperations.add(new NodeOperation(this, Scene.NodeOperationType.Insert));
			mParent = parent;
		}
	}

	/**
	 * @return this nodes unique scene id.
	 */
	public String getName() {
		return mName;
	}

	/**
	 * Sets the unique node scene id.
	 * 
	 * @param name
	 *            The identifier.
	 */
	public void setName(String name) {
		if (!mName.isEmpty() && mScene != null) {
			mScene.mNodeLookup.remove(mName);

		}
		if (mScene != null)
			mScene.mNodeLookup.put(name, this);
		mName = name != null ? name : "";
	}

	/**
	 * Sets a user defined property.
	 * 
	 * @param name
	 *            The string key of the property.
	 * @param value
	 *            Any user defined data.
	 */
	public void setProperty(String name, Object value) {
		mProperties.put(name, value);
	}

	/**
	 * Gets a user defined property.
	 * 
	 * @param name
	 *            The property to retrieve.
	 * @return the property associated with the given key.
	 */
	@SuppressWarnings("unchecked")
	public <T> T getProperty(String name) {
		return (T) mProperties.get(name);
	}

	/**
	 * Checks for an user-defined property.
	 * 
	 * @param name
	 *            The property to check for.
	 * @return true if the property exists, otherwise false.
	 */
	public boolean hasProperty(String name) {
		return mProperties.containsKey(name);
	}

	/**
	 * Attaches a new renderable model to this scene node.
	 * 
	 * @param mi
	 *            The RenderableProvider to attach (e.g. a ModelInstance)
	 */
	public void attachRenderable(RenderableProvider rp) {
		mRenderables.add(rp);
	}

	/**
	 * Detaches a previously attached model from this scene node.
	 * 
	 * @param mi
	 *            The RenderableProvider to detach (e.g. a ModelInstance)
	 */
	public void detachRenderable(RenderableProvider rp) {
		mRenderables.removeValue(rp, true);
	}

	/**
	 * Implementation of the Disposable interface. Will also dispose children.
	 */
	public void dispose() {
		if (mParent != null) {
			mParent.mChildren.removeValue(this, true);
			mParent.mUpdateBoundingBox = true;
		}
		removeAndDisposeAllChildren();
	}

	public void removeAndDisposeAllChildren() {
		for (SceneNode n : mChildren) {
			n.mParent = null;
			n.dispose();
		}
	}

	/**
	 * Gets an array of all children. The array needs to stay in an immutable state, so no insertions/removals should be made manually.
	 * 
	 * @return Returns a list of child scene nodes.
	 * @see removeAllChildren()
	 * @see remove()
	 */
	public Array<SceneNode> getChildren() {
		return mChildren;
	}

	/**
	 * Gets the local boundaries as defined by the user. No world transformations or child boundaries are taken into account.
	 * 
	 * @return the scene node boundaries in local transformation space.
	 * @see getBoundingBox()
	 */
	public BoundingBox getLocalBounds() {
		return mLocalBounds;
	}

	/**
	 * Gets the world-transformed bounding box. The result is prevailing and thus guaranteed to be correct.
	 * 
	 * @return the world-transformed bounding box of this node.
	 */
	public BoundingBox getWorldBounds() {
		update();
		return mWorldBounds;
	}

	/**
	 * Gets this scene nodes parent if present.
	 * 
	 * @return The parent scene node.
	 */
	public SceneNode getParent() {
		return mParent;
	}

	/**
	 * Gets all attached renderables. The array needs to stay in a mutable state, so no insertions/removals should be made manually.
	 * 
	 * @return Returns a list of attached geometry models.
	 */
	public Array<RenderableProvider> getRenderables() {
		return mRenderables;
	}

	/**
	 * Gets the scene this node belongs to.
	 * 
	 * @return The scene this node is associated to.
	 */
	public Scene getScene() {
		return mScene;
	}

	public void init() {

	}

	/**
	 * Invalidates the scene nodes transform or bounding box.
	 * 
	 * @param invalidateTransform
	 * @param invalidateBoundingBox
	 */
	public void invalidate(boolean invalidateTransform, boolean invalidateBoundingBox) {

		if (mUpdateTransform && mUpdateBoundingBox) // Already fully invalidated
			return;

		if (!mUpdateTransform && invalidateTransform) {
			mUpdateTransform = true;
			for (int i = 0; i < mChildren.size; i++) { // Cause children to reset invalidate their transformation
				mChildren.get(i).invalidate(true, false);
			}
		}

		if (!mUpdateBoundingBox && invalidateBoundingBox) {
			mUpdateBoundingBox = true;
		}

		if (mParent != null) { // Any modification in a leaf causes the parent to reset its bounding box.
			mParent.invalidate(false, true);
		}
	}

	/**
	 * Checks for a node inheritance.
	 * 
	 * @param n
	 *            the potential parent or ancestor node.
	 * @return true if n is an ancestor of this node, otherwise false.
	 */
	public boolean isChildOf(SceneNode n) {
		if (mParent == null)
			return false;
		if (mParent == n)
			return true;
		return mParent.isChildOf(n);
	}

	/**
	 * Returns the nodes visibility state. Note: Children of invisible nodes will not be rendered or updated.
	 * 
	 * @return true if the node is visible, otherwise false.
	 */
	public boolean isVisible() {
		return mVisible;
	}

	public Matrix4 getWorldTransform() {
		if (mUpdateTransform)
			updateTransform(true);
		return mWorldTransform;
	}

	public void lookAt(float x, float y, float z, TransformationSpace relativeTo) {
		lookAt(new Vector3(x, y, z), relativeTo);
	}

	/**
	 * Performs a lookAt-Transformation of this nodes orientation.
	 * 
	 * @param target
	 * @param relativeTo
	 *            Tgl20he transformation space in which the operation should take place.
	 */
	final public void lookAt(Vector3 target, TransformationSpace relativeTo) {
		lookAt(target, relativeTo, Vector3.Z.cpy().scl(-1));
	}

	/**
	 * Performs a lookAt-Transformation of this nodes orientation.
	 * 
	 * @param target
	 * @param relativeTo
	 *            The transformation space in which the operation should take place.
	 * @paran localDirectionVector The local direction vector of this node, meaning the orientation of the nodes front to face towards the target. By default this is the nodes negative z-axis.
	 */
	public void lookAt(Vector3 target, TransformationSpace relativeTo, Vector3 localDirectionVector) {

		// Calculate ourself origin relative to the given transform space
		Vector3 origin = new Vector3();
		switch (relativeTo) {
		default: // Just in case
		case World:
			origin = getWorldTransform().getTranslation(origin);
			break;
		case Parent:
			origin = mPosition;
			break;
		case Local:
			origin = Vector3.Zero;
			break;
		}

		setDirection(target.cpy().sub(origin), relativeTo, localDirectionVector);
	}

	/**
	 * Moves the node relative to its current position (in local transformation space)
	 * 
	 * @param p
	 *            The local translation vector to add to the current position.
	 */
	public void move(Vector3 p) {
		mPosition.add(p);
		invalidate(true, false);
	}

	public void move(float x, float y, float z) {
		mPosition.add(x, y, z);
		invalidate(true, false);
	}

	/**
	 * Detaches this node from its parent. Will not dispose attached children, you will need to call dispose() for that.
	 * 
	 * @see dispose()
	 */
	public void remove() {
		if (mParent == null)
			return;

		mScene.mQueuedNodeOperations.add(new NodeOperation(this, Scene.NodeOperationType.Remove));
		mParent.mChildren.removeValue(this, true);
		mParent.invalidate(false, true);
		mParent = null;
	}

	/**
	 * Removes all child scene nodes from this node. Will not dispose attached children, you will need to call dispose() for that.
	 * 
	 * @see dispose()
	 */
	public void removeAllChildren() {

		for (SceneNode child : mChildren) {

			mScene.mQueuedNodeOperations.add(new NodeOperation(child, Scene.NodeOperationType.Remove));
			child.mParent = null;

		}

		invalidate(false, true);
		mChildren.clear();

	}

	/**
	 * Removes all renderables from this node.
	 */
	public void removeAllRenderables() {
		mRenderables.clear();
	}

	/**
	 * Renders the node.
	 * 
	 * @param batch
	 * @param env
	 */
	public void render(ModelBatch modelBatch, Environment env) {

		update();

		TransformedModelBatch batch = (TransformedModelBatch) modelBatch;

		// for (RenderableProvider rp : mRenderables) {

		for (int i = 0; i < mRenderables.size; i++) {
			RenderableProvider rp = mRenderables.get(i);
			batch.render(rp, env, mWorldTransform);
		}

		if (mDebugBoundsModelInstance != null && mRenderDebugBounds) {

			batch.render(mDebugBoundsModelInstance);
		}

	}

	public void setDirection(Vector3 dir, TransformationSpace space) {
		setDirection(dir, space, Vector3.Z.cpy().scl(-1));
	}

	/**
	 * Sets the current direction of this node.
	 * 
	 * @param dir
	 *            the directional vector to look at.
	 * @param space
	 *            the transformation space. Transformations can be local, parent or world oriented.
	 */
	public void setDirection(Vector3 dir, TransformationSpace space, Vector3 localDirectionVector) {

		Vector3 target = dir.cpy().nor();

		switch (space) {
		case Parent:
			if (mParent != null) {
				Quaternion q = mTmpQuat;
				mParent.getWorldTransform().getRotation(q);
				target.mul(q);
			}
			break;
		case Local:
			target.mul(mOrientation);
			break;
		case World:
			break;
		}

		Quaternion targetOrientation = mTmpQuat.set(mOrientation);
		Vector3 currentDir = localDirectionVector.cpy().mul(targetOrientation);

		if (currentDir.cpy().add(target).len2() < 0.00005) {
			// 180 degree turn, invert the orientation quaternion.
			targetOrientation.set(-mOrientation.y, -mOrientation.z, mOrientation.w, mOrientation.x);
		} else {
			// new Quaternion().
			// targetOrientation.set
			Quaternion rotQuat = com.vectornoid.tapdoor.util.QuaternionUtils.getRotationTo(currentDir, target, mTmpQuat);
			targetOrientation = rotQuat.mul(mOrientation);
		}

		setOrientation(targetOrientation);

	}

	/**
	 * Gets the node orientation in local transformation space.
	 * 
	 * @return this nodes local orientation
	 */
	public Quaternion getOrientation() {
		return mOrientation;
	}

	/**
	 * Gets the node position in local transformation space.
	 * 
	 * @return this nodes local position.
	 */
	public final Vector3 getPosition() {
		return mPosition;
	}

	/**
	 * Gets the node position in the given transformation space.
	 * 
	 * @return this nodes position in the given transformation space.
	 */
	public final Vector3 getPosition(TransformationSpace ts) {

		if (ts == TransformationSpace.World) {
			return getWorldTransform().getTranslation(new Vector3());
		}

		return mPosition;

	}

	public void setLocalBounds(float xmin, float ymin, float zmin, float xmax, float ymax, float zmax) {
		mLocalBounds.set(new Vector3(xmin, ymin, zmin), new Vector3(xmax, ymax, zmax));
		invalidate(false, true);
	}

	public void setLocalBounds(float xmax, float ymax, float zmax) {
		setLocalBounds(0, 0, 0, xmax, ymax, zmax);
	}

	public void setLocalBounds(BoundingBox extents) {

		if (extents != null)
			mLocalBounds.set(extents);
		else
			mLocalBounds.clr();

		invalidate(false, true);
	}

	public void setOrientation(Quaternion q) {
		mOrientation.set(q);
		invalidate(true, false);
	}

	public void setOrientation(float yaw, float pitch, float roll) {
		mOrientation.setEulerAngles(yaw, pitch, roll);
		invalidate(true, false);
	}

	public void yaw(float amount) {
		mOrientation.mul(new Quaternion().setEulerAngles(amount, 0, 0));
		invalidate(true, false);
	}

	public void pitch(float amount) {
		mOrientation.mul(new Quaternion().setEulerAngles(0, amount, 0));
		invalidate(true, false);
	}

	public void roll(float amount) {
		mOrientation.mul(new Quaternion().setEulerAngles(0, 0, amount));
		invalidate(true, false);
	}

	public void setPosition(Vector3 p) {
		mPosition.set(p);
		invalidate(true, false);
	}

	public void setPosition(float x, float y, float z) {
		mPosition.set(x, y, z);
		invalidate(true, false);
	}

	public void setPosition(Vector3 p, TransformationSpace ts) {
		switch (ts) {
		case Local:
			setPosition(p);
			break;
		case Parent:
			break;
		case World:
			if (mParent != null) {
				Vector3 parentWorldPosition = mParent.getWorldTransform().getTranslation(new Vector3());
				setPosition(p.sub(parentWorldPosition));
			}
			break;
		}
	}

	public void setScale(Vector3 s) {
		mScale.set(s);
		invalidate(true, false);
	}
	
	public void setScale(float x, float y, float z) {
		mScale.set(x,y,z);
		invalidate(true, false);
	}

	public Vector3 getScale() {
		return mScale;
	}

	/**
	 * @param render
	 *            if set to true, the scene nodes boundaries will be renderered. Mainly for debugging purposes.
	 */
	public void setRenderBoundingBox(boolean render) {
		mRenderDebugBounds = render;
		if (!render && mDebugBoundsModel != null) {
			// mDebugBoundsModel.dispose();
			// mDebugBoundsModel = null;
			mDebugBoundsModelInstance = null;
		} else {
			invalidate(false, true);
		}
	}

	public boolean getRenderBoundingBox() {
		return mRenderDebugBounds;
	}

	/**
	 * Shows or hides this node. Only visible nodes will be updated.
	 * 
	 * @param visible
	 */
	public void setVisible(boolean visible) {
		if (mVisible != visible) {
			mVisible = visible;
			mScene.mQueuedNodeOperations.add(new NodeOperation(this, NodeOperationType.VisibilityChanged));
			// mScene.mCullingStrategy.nodeVisibilityChanged(this);
		}
	}

	/**
	 * Updates the nodes transformation and bounding box data.
	 */
	public void update() {

		if (!mRenderDebugBounds && mDebugBoundsModel != null) {
			mDebugBoundsModel.dispose();
			mDebugBoundsModel = null;
		}

		if (mUpdateTransform)
			updateTransform(true);
		if (mUpdateBoundingBox) {
			updateBoundingBox(true);
		}

	}

	/**
	 * Updates the nodes transformation matrix.
	 * 
	 * @param updateLocalTransform
	 *            a boolean indicating whether the local transformation matrix should be recalculated first.
	 */
	public void updateTransform(boolean updateLocalTransform) {

		if (mParent != null && mParent.mUpdateTransform) {
			mParent.updateTransform(false);
		}

		if (updateLocalTransform)
			mLocalTransform.set(mOrientation).setTranslation(mPosition).scale(mScale.x, mScale.y, mScale.z);

		if (mParent == null) {
			mWorldTransform.set(mLocalTransform);
		}

		else {
			mWorldTransform.set(mParent.mWorldTransform);
			mWorldTransform.mul(mLocalTransform);
		}

		if (mCollider != null) {
			mCollider.updateTransform(mWorldTransform);
		}

		mUpdateTransform = false;
	}

	/**
	 * Updates the scene nodes bounding box.
	 */
	public void updateBoundingBox(boolean updateChildExtents) {
		
		final float x0 = mLocalBounds.min.x, y0 = mLocalBounds.min.y, z0 = mLocalBounds.min.z, x1 = mLocalBounds.max.x, y1 = mLocalBounds.max.y, z1 = mLocalBounds.max.z;
		mTmpVec.set(x0, y0, z0).mul(mWorldTransform);
		mWorldBounds.min.set(mTmpVec);
		mTmpVec.set(x1, y1, z1).mul(mWorldTransform);
		mWorldBounds.max.set(mTmpVec);
		
		mUpdateBoundingBox = false;

		for (int i = 0; i < mChildren.size; i++) {
			SceneNode n = mChildren.get(i);

			if (updateChildExtents) {
				n.updateTransform(n.mUpdateTransform);
				n.updateBoundingBox(n.mUpdateBoundingBox);
			}

			if (!mWorldBounds.isValid()) { // We do not have valid world bounds, use the child bounds instead.
				mWorldBounds.set(n.mWorldBounds);
			} else if (n.mWorldBounds.isValid()) { // Only extent the boundaries if the child world bounds are ok.
				mWorldBounds.ext(n.mWorldBounds);
			}
		}

		if (mRenderDebugBounds) {
			buildDebugBoundsModel();
		}

	}

	/**
	 * Rebuilds the bounding box debug model
	 */
	private void buildDebugBoundsModel() {

		if (mDebugBoundsModel != null) {
			mDebugBoundsModel.dispose();
			mDebugBoundsModel = null;
		}

		ModelBuilder mb = new ModelBuilder();

		mb.begin();
		MeshPartBuilder pb = mb.part("boundingBox", GL20.GL_LINES, Usage.Position, new Material(ColorAttribute.createDiffuse(mDebugColor)));

		pb.line(mWorldBounds.min, mWorldBounds.max);

		float x0 = mWorldBounds.min.x;
		float x1 = mWorldBounds.max.x;

		float y0 = mWorldBounds.min.y;
		float y1 = mWorldBounds.max.y;

		float z0 = mWorldBounds.min.z;
		float z1 = mWorldBounds.max.z;

		pb.box(new Vector3(x0, y0, z0), new Vector3(x0, y1, z0), new Vector3(x1, y0, z0), new Vector3(x1, y1, z0), new Vector3(x0, y0, z1), new Vector3(x0, y1, z1), new Vector3(x1, y0, z1), new Vector3(x1, y1, z1));

		mDebugBoundsModel = mb.end();
		mDebugBoundsModelInstance = new ModelInstance(mDebugBoundsModel);
	}

	@Override
	public String toString() {
		return (!mName.isEmpty() ? mName : super.toString());
	}

	@Override
	public void serialize(SerializedWriter target) {

		target.write(mName);

		target.write(mPosition);
		target.write(mOrientation);
		target.write(mLocalBounds);

		target.write(mChildren.size);

		for (SceneNode child : mChildren) {
			if (mScene.mSerializationListener != null)
				mScene.mSerializationListener.serializeNode(target, child);
			child.serialize(target);
		}

	}

	@Override
	public void unserialize(SerializedReader source) {

		setName(source.readString() + "*");

		source.readVector3(mPosition);
		source.readQuaternion(mOrientation);
		source.readBoundingBox(mLocalBounds);

		int numChildren = source.readInt();

		removeAllChildren();

		for (int i = 0; i < numChildren; i++) {
			SceneNode n;
			if (mScene.mSerializationListener != null)
				n = mScene.mSerializationListener.unserializeNode(source, this);
			else
				n = new SceneNode(this);
			n.unserialize(source);
		}

		invalidate(true, true);
		update();
	}

	public ModelInstance getBoundingBoxModelInstance() {
		// TODO Auto-generated method stub
		return mDebugBoundsModelInstance;
	}

}
