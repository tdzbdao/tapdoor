package com.vectornoid.tapdoor.gfx.scene3d;

/**
 *
 * Transformation space for scene node operations.
 * 
 * @see SceneNode.lookAt()
 * @see SceneNode.setDirection()
 * @see SceneNode.setPosition()
 * 
 * @author void (tdzbdao@gmail.com)
 *
 */

public enum TransformationSpace {

	Local,
	Parent,
	World,
	
}
