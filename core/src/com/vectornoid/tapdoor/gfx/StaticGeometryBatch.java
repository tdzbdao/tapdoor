package com.vectornoid.tapdoor.gfx;

import com.badlogic.gdx.graphics.Mesh;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.utils.MeshPartBuilder;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Disposable;

/**
 * 
 * @author void
 *
 * Static geometry batches are sets of meshes that are, along with their respective transforms, statically
 * compiled as a single mesh to reduce draw calls.
 * 
 * Basically, any mesh that is considered non-dynamic (e.g. scenery) should be treated as static geometry.
 *
 */
public class StaticGeometryBatch implements Disposable {

	private Array<Mesh> mMeshesToCombine = new Array<>();
	private Array<Matrix4> mMeshTransforms = new Array<>();
	
	public StaticGeometryBatch() {
		ModelBatch b;
		MeshPartBuilder mpb;
	}
		
	/**
	 * Resets the static geometry batch. Should be called before add().
	 */
	public void begin() {
		dispose();
	}

	/**
	 * Adds a new model to the static geometry.
	 * @param m
	 */
	public void add( ModelInstance m ) {
	
		for(Mesh mesh : m.model.meshes ) {
			mMeshesToCombine.add( mesh );
			mMeshTransforms.add(m.transform);
		}
	}
	
	/**
	 * Finalizes the static geometry cache. Needs to be called after the last call to add().
	 * @return A combined mesh of all static geometry models.
	 */
	public Mesh end() {
		
		Mesh[] meshes = mMeshesToCombine.toArray(Mesh.class);
		Matrix4[] transforms = mMeshTransforms.toArray(Matrix4.class);
		
		Mesh combinedGeometry = Mesh.create(true, meshes, transforms);
		
		dispose();
		
		return combinedGeometry;
	}

	/**
	 * Clears the model cache.
	 */
	@Override
	public void dispose() {
		
		mMeshesToCombine.clear();
	}
	
}
