package com.vectornoid.tapdoor.system;

import java.util.ArrayList;

import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenManager;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.vectornoid.tapdoor.gfx.sprite.SpriteManager;
import com.vectornoid.tapdoor.l10n.LocalizationManager;
import com.vectornoid.tapdoor.system.tween.Vector3TweenAccessor;

/**
 * 
 * This class holds global objects that need to be shared between most parts of the actual application. 
 * Globals is implemented using a singleton pattern.
 * 
 * @author void (tdzbdao@gmail.com)
 */

public class Globals {

	private static Globals mSingleton = null;

	public static FontManager getFontManager() {
		assert (mSingleton != null);

		return mSingleton.mFontManager;
	}
	
	public static SpriteManager getSpriteManager() {
		assert (mSingleton != null);

		return mSingleton.mSpriteManager;
	}

	public static InputMultiplexer getInputMultiplexer() {
		assert (mSingleton != null);

		return mSingleton.mInputMultiplexer;
	}

	public static PlatformManager getPlatformManager() {
		assert (mSingleton != null);

		return mSingleton.mPlatformManager;
	}
	
	public static TweenManager getTweenManager() {
		assert (mSingleton != null);

		return mSingleton.mTweenManager;
	}
	
	public static LocalizationManager getLocalizationManager() {
		assert (mSingleton != null);

		return mSingleton.mLocalizationManager;
	}

	/**
	 * Performs common periodically tasks.
	 */
	public static void update() {
		if (mSingleton == null)
			return;
		if (!mSingleton.mInputMultiplexerSet) {
			Gdx.app.log("TapDoor", "Set input multiplexer");
			Gdx.input.setInputProcessor(mSingleton.mInputMultiplexer);
			mSingleton.mInputMultiplexerSet = true;
		}

		mSingleton.mTweenManager.update(Gdx.graphics.getDeltaTime());
		mSingleton.mPlatformManager.update();
		
	}

	private PlatformManager mPlatformManager;
	private FontManager mFontManager;
	private SpriteManager mSpriteManager;
	private InputMultiplexer mInputMultiplexer;
	private TweenManager mTweenManager;
	private LocalizationManager mLocalizationManager;

	private boolean mInputMultiplexerSet = false;

	private static float mBoxToWorld = 10.0f; // Box2D unit scale factor, 1 unit in Box2D space equals 10 units in world space.

	private static float mWorldToBox = 1.0f / mBoxToWorld;
	private static ArrayList<ApplicationResizeListener> mApplicationResizeListeners = new ArrayList<>();

	/**
	 * Should be called as soon as possible once the application has booted.
	 * 
	 * @param platformManager
	 *            A suitable platform for the selected architecture.
	 */
	public Globals(PlatformManager platformManager) {
		assert (mSingleton == null);
		mSingleton = this;
		mPlatformManager = platformManager;

		mInputMultiplexer = new InputMultiplexer();
		mFontManager = new FontManager("data/font");
		mSpriteManager = new SpriteManager();
		mTweenManager = new TweenManager();
		mLocalizationManager = new LocalizationManager();
		
		Tween.registerAccessor(Vector3.class, new Vector3TweenAccessor());

		// Common libGDX setup
		//GLTexture.setEnforcePotImages(false); // Disable power of two texture restrictions.
	}

	/**
	 * Box2D <-> world coordinate transformation methods.
	 */

	public static float boxToWorld(float value) {
		return value * mBoxToWorld;
	}

	public static float[] boxToWorld(float[] values) {
		for (int i = 0; i < values.length; i++) {
			values[i] = boxToWorld(values[i]);
		}
		return values;
	}

	public static Vector2 boxToWorld(Vector2 value) {
		return value.scl(mBoxToWorld);
	}

	public static Vector3 boxToWorld(Vector3 value) {
		return value.scl(mBoxToWorld);
	}

	public static float getBoxToWorldFactor() {
		return mBoxToWorld;
	}

	public static float worldToBox(float value) {
		return mWorldToBox * value;
	}

	public static float[] worldToBox(float[] values) {
		for (int i = 0; i < values.length; i++) {
			values[i] = worldToBox(values[i]);
		}
		return values;
	}

	public static Vector2 worldToBox(Vector2 value) {
		return value.scl(mWorldToBox);
	}

	public static Vector3 worldToBox(Vector3 value) {
		return value.scl(mWorldToBox);
	}

	public static void registerApplicationResizeListener(ApplicationResizeListener l) {
		mApplicationResizeListeners.add(l);
	}
	
	public static void invokeApplicationResizeEvent(int w, int h) {
		for(ApplicationResizeListener a : mApplicationResizeListeners) {
			a.resize(w, h);
		}
	}
}
