package com.vectornoid.tapdoor.system;

import java.util.LinkedList;

/**
 * 
 * @author void (tdzbdao@gmail.com)
 * 
 * Abstract class for task based execution. 
 * Concrete implementation of this class is
 * built-target dependent.
 * 
 * @see LinearTask 
 * @see ThreadedTask 
 *
 */
public abstract class Task {

	static public interface DoneCallback {
		public void done();
	}
	static public interface StepCallback {
		public boolean run(int currentIteration);
	}
	
	protected LinkedList<StepCallback> mSteps = new LinkedList<StepCallback>();
		
	protected TaskResult mResult = new TaskResult();
	
	public Object UserData = null;
	
	public final TaskResult getResult() {
		return mResult;
	}
		
	abstract public void perform(DoneCallback callback);
			
	public Task step(StepCallback stepCallback)
	{
		mSteps.add(stepCallback);
		return this;
	}
	
}
