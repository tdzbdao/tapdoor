package com.vectornoid.tapdoor.system;

import java.nio.ByteBuffer;
import java.util.Collection;

import com.badlogic.gdx.math.Quaternion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.math.collision.BoundingBox;
import com.badlogic.gdx.utils.Array;

abstract public class SerializedWriter {

	public void write(int x) {
		write(ByteBuffer.allocate(4).putInt(x).array(), 0, 4);
	}

	public void write(short x) {
		write(ByteBuffer.allocate(2).putShort(x).array(), 0, 2);
	}

	public void write(float x) {
		write(ByteBuffer.allocate(4).putFloat(x).array(), 0, 4);
	}

	public void write(String x) {
		
		if(x.length() > 0) {
			byte[] r = x.getBytes();
			write(r.length);
			write(r, 0, r.length);
		}
		else
			write((int)0);
		
	}

	public void write(Vector3 x) {
		write(x.x);
		write(x.y);
		write(x.z);
	}

	public void write(Vector2 x) {
		write(x.x);
		write(x.y);
	}

	public void write(Quaternion x) {
		write(x.x);
		write(x.y);
		write(x.z);
		write(x.w);
	}

	public void write(BoundingBox x) {
		write(x.min);
		write(x.max);
	}
	
	public <T extends Serializable> void write(Array<T> array) {
		write(array.size);
		for(T t : array) {
			t.serialize(this);
		}
	}
	
	public <T extends Serializable> void write(Collection<T> col) {
		write(col.size());
		for(T t : col) {
			t.serialize(this);
		}
	}

	abstract public void write(byte[] data, int offset, int len);
}
