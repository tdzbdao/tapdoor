package com.vectornoid.tapdoor.system;

import com.badlogic.gdx.Gdx;

/**
 * 
 * @author void (tdzbdao@gmail.com)
 *
 * A task that runs in a seperate thread and thus is very well suited for background loading of resources.
 * Keep in mind that loading textures requires the main rendering thread!
 *
 */
public class ThreadedTask extends Task {

	@Override
	public void perform(final DoneCallback callback) {
		// TODO Auto-generated method stub

		new Thread(new Runnable(){

			@Override
			public void run() {
				Gdx.app.log("System", "Spawning task thread");
				
				// TODO Auto-generated method stub
				while(!mSteps.isEmpty())
				{
					StepCallback step = mSteps.poll();
					mResult.mIterations = 0;
					while(!step.run(mResult.mIterations++)){
					};
					// DONE
					mResult.mFinished = true;
					callback.done();
				}
			}
			
		}).start();
	}

}
