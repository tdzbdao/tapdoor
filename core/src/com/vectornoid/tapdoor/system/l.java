package com.vectornoid.tapdoor.system;

/**
 * 
 * @author void
 *
 * A short-hand static class for accessing the global localization manager. 
 *
 */

public class l {

	public static String get(String source, String group) {
		return Globals.getLocalizationManager().getString(source, group);
	}
	
	public static String get(String source) {
		return Globals.getLocalizationManager().getString(source);
	}
		
	public static String format(String source, String group, Object ...args) {
		String s = Globals.getLocalizationManager().getString(source, group);
		if(s == null)
			return "";
		
		return String.format(s, args);
	}
}
