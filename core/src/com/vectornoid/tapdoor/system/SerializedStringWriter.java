package com.vectornoid.tapdoor.system;

public class SerializedStringWriter extends SerializedWriter {

	private String mResult = "";
	
	@Override
	public void write(byte[] data, int offset, int len) {
		mResult += new String(data, offset, len);
	}
	
	@Override
	public String toString() {
		
		return mResult;
	}
	
}
