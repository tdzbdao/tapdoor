package com.vectornoid.tapdoor.system.tween;

import aurelienribon.tweenengine.TweenAccessor;

import com.badlogic.gdx.math.Vector3;

/**
 * @author void
 * 
 * Tween accessor for Vector3 class.
 *
 */
public class Vector3TweenAccessor implements TweenAccessor<Vector3> {

	private Vector3 vec3 = new Vector3(0, 0, 0);

	@Override
	public int getValues(Vector3 target, int tweenType, float[] returnValues) {
		
		returnValues[0] = target.x;
		returnValues[1] = target.y;
		returnValues[2] = target.z;
		return 3;
	}

	@Override
	public void setValues(Vector3 target, int tweenType, float[] newValues) {
		
		target.x = newValues[0];
		target.y = newValues[1];
		target.z = newValues[2];
	}
}
