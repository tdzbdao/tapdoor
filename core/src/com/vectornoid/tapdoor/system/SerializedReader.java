package com.vectornoid.tapdoor.system;

import java.nio.ByteBuffer;

import com.badlogic.gdx.math.Quaternion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.math.collision.BoundingBox;

public abstract class SerializedReader {

	public int readInt() {
		ByteBuffer buf = ByteBuffer.allocate(4);
		read(buf, 0, 4);		
		return ((ByteBuffer) buf.rewind()).asIntBuffer().get();
	}

	public short readShort() {
		ByteBuffer buf = ByteBuffer.allocate(2);
		read(buf, 0, 2);		
		return ((ByteBuffer) buf.rewind()).asShortBuffer().get();
	}

	public float readFloat() {
		ByteBuffer buf = ByteBuffer.allocate(4);
		read(buf, 0, 4);		
		return ((ByteBuffer) buf.rewind()).asFloatBuffer().get();
	}

	public String readString() {
		int len = readInt();
		if(len == 0)
			return new String();
		byte b[] = new byte[len];
		read(b, 0, len);
		
		String result = new String(b, 0, len);
		
		return result;
	}

	public Vector3 readVector3(Vector3 x) {
		x.x = readFloat();
		x.y = readFloat();
		x.z = readFloat();
		return x;
	}

	public Vector2 readVector2(Vector2 x) {
		x.x = readFloat();
		x.y = readFloat();
		return x;
	}

	public Quaternion readQuaternion(Quaternion x) {
		x.x = readFloat();
		x.y = readFloat();
		x.z = readFloat();
		x.w = readFloat();
		return x;
	}

	public BoundingBox readBoundingBox(BoundingBox x) {
		readVector3(x.min);
		readVector3(x.max);
		return x;
	}
	
	abstract public void read(byte[] target, int offset, int len);
	
	public void read(ByteBuffer target, int offset, int len) {
		read(target.array(), offset, len);
	}

}
