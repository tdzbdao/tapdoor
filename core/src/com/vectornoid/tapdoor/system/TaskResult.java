package com.vectornoid.tapdoor.system;

import com.badlogic.gdx.math.MathUtils;

/**
 *
 * @author void (tdzbdao@gmail.com)
 * 
 * A task result can be used to check for a previously created tasks current state.
 * As a common use case one could spawn a task somewhere and periodically check its state during the render loop.
 *
 */
public class TaskResult {

	boolean mFinished = false;
	int mPredictedIterations = 0;
	int mIterations = 0;
	
	public boolean finished() {
		return mFinished;
	}
	
	public int getIterations() {
		return mIterations;
	}
	
	public float getPercentDone() {
		if(!mFinished && mPredictedIterations > 0)
			return MathUtils.clamp((float)mIterations / mPredictedIterations, 0.0f, 1.0f);
		return mFinished ? 1.0f : 0.0f;
	}
	
	public void setPredictedIterations(int iterations) {
		mPredictedIterations = iterations;
	}
	 
	
	
}
