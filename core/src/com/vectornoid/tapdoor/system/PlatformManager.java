package com.vectornoid.tapdoor.system;

/**
 * 
 * @author void (tdzbdao@gmail.com)
 *
 * The platform manager is an interface for platform specific stuff.
 * 
 */
public interface PlatformManager {
	
	/**
	 * 
	 * Updates platform specific stuff. Is called within the Globals update method.
	 * 
	 */
	public void update();
	
	/**
	 * 
	 * @param runInRenderThread a linear task is returned when this task should run inside the rendering loop (this is the only task type available on HTML!)
	 * 
	 * @return a new task
	 */
	Task createTask(boolean runInRenderThread);
			
}
