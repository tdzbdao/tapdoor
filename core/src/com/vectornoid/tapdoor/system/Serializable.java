package com.vectornoid.tapdoor.system;

/**
 * @author void
 *
 * Serializable object interface.
 *
 */
public interface Serializable {

	/**
	 * Serializes this object to a SerializedWriter
	 * @param writer
	 */
	public void serialize(SerializedWriter writer);
	
	/**
	 * Deserializes this object from a SerializedReader
	 * @param reader
	 */
	public void unserialize(SerializedReader reader);
	
}
