package com.vectornoid.tapdoor.system;

import java.util.LinkedList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.TimeUtils;

/**
 * 
 * @author void (tdzbdao@gmail.com)
 *
 * A linear task runs inside the main thread. This is the only available task on HTML5 targets.
 */

public class LinearTask extends Task {

	public static short MaxExecutionTime = 1; 
	private static LinkedList<LinearTask> mActiveTasks = new LinkedList<LinearTask>();
	public static void update()
	{		
		if(mActiveTasks.isEmpty())
			return;
		LinearTask first = mActiveTasks.poll();
		LinearTask current = first;
		
		while(true)
		{
			if(!current.performSteps())
			{
				mActiveTasks.add(current);
			}
			else
			{
				current.mResult.mFinished = true;
				current.mCallback.done();
			}

			if(!mActiveTasks.isEmpty())
			{			
				current = mActiveTasks.poll();
				if(current == first)
				{	
					mActiveTasks.add(first);
					break;
				}
			}
			else
			{
				break;
			}
		}
		
	}
	
	private DoneCallback mCallback;
	
	@Override
	public void perform(DoneCallback callback) {
		
		Gdx.app.log("System", "Spawning linear task");
	
		mCallback = callback;
		mActiveTasks.add(this);
	}
	
	private boolean performSteps()
	{		
		long now = TimeUtils.millis();
		short delta = 0;
			
		while(delta < MaxExecutionTime && !mSteps.isEmpty())
		{
			if(mSteps.peek().run(mResult.mIterations++))
			{
				mSteps.poll();
				mResult.mIterations = 0;
			}
			delta = (short) (TimeUtils.millis() - now);
		}
		
		if(mSteps.isEmpty())
		{
			return true;
		}
		return false;
	}

}
