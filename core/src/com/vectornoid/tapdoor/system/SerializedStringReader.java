package com.vectornoid.tapdoor.system;

public class SerializedStringReader extends SerializedReader {

	private byte[] mData;
	private int mCursor = 0;
	
	public SerializedStringReader(String data) {
		mData = data.getBytes();
	}
	
	@Override
	public void read(byte[] target, int offset, int len) {
		
		for(int i=0;i<len;i++) {
			target[offset+i] = mData[mCursor+i];
		}
		mCursor += len;
	}

}
