package com.vectornoid.tapdoor.system;

import java.util.HashMap;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;

/**
 * 
 * @author void (tdzbdao@gmail.com)
 * 
 * A quick & dirty font manager class.
 * 
 * @TODO: This needs to be replaced by a proper asset management.
 *
 */

public class FontManager {

	private HashMap<String, BitmapFont> mLoadedFonts = new HashMap<String, BitmapFont>();
	private String mFontFolder;
	
	public FontManager(String fontFolder) {
		 mFontFolder = fontFolder;
	}
	
	public BitmapFont getFont(String fontname) {
		
		if(mLoadedFonts.containsKey(fontname)) {
			return mLoadedFonts.get(fontname);
		}
				
		String fontBase = mFontFolder + "/" + fontname;
		BitmapFont fnt = new BitmapFont(
				Gdx.files.internal(fontBase + ".fnt"),
				Gdx.files.internal(fontBase + ".png"),
				false
				);
				
		mLoadedFonts.put(fontname, fnt);
		return fnt;
	}
	
	
	
}
