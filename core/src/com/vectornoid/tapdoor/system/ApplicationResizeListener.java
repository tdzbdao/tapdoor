package com.vectornoid.tapdoor.system;

/**
 * @author void
 *
 * Listener interface for application window resize events.
 *
 */
public interface ApplicationResizeListener {

	public void resize(int width, int height);
}
