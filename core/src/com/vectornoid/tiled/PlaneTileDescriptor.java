package com.vectornoid.tiled;

import com.badlogic.gdx.graphics.VertexAttributes.Usage;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder;

/**
 * @author void
 * 
 * A tile descriptor that defines a plane-shaped tile.
 *
 */
public class PlaneTileDescriptor extends TileDescriptor {

	Model mModel;
	
	public PlaneTileDescriptor(TileConfig cfg) {
	
		float sz = cfg.getTileSize();
		
		ModelBuilder mb = new ModelBuilder();
		mModel = mb.createRect(0, 0, 0, sz, sz, 0, sz, sz, 0, 0, sz, 0, 0, 0, -1, new Material(), Usage.Position | Usage.Normal | Usage.TextureCoordinates);		
	}

	@Override
	public Model getModel() {
		return mModel;
	}
	
}

