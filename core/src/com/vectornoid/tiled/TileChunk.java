package com.vectornoid.tiled;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Mesh;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder;
import com.vectornoid.tapdoor.gfx.StaticGeometryBatch;
import com.vectornoid.tapdoor.gfx.scene3d.SceneNode;

/**
 * @author void
 *
 * A tile chunk is a finite piece of a tiled map.
 * By using a background loading and unloading strategy, tile chunks can be utilized to simulate an inifite world that
 * is streaming from a local drive (or any remote source as well).
 * 
 */
public class TileChunk extends SceneNode {

	class TileMeshInfo {
		ModelInstance model;
	}

	TileMeshInfo[] mTileMeshInfos;

	TileConfig mConfig;
	ModelInstance mModelInstance = null;
	Mesh mStaticGeometryMesh = null;

	private void initTiles() {

		mTileMeshInfos = new TileMeshInfo[mConfig.getChunkSize() * mConfig.getChunkSize()];
		for (int i = 0; i < mTileMeshInfos.length; i++) {
			mTileMeshInfos[i] = new TileMeshInfo();
		}

	}

	public TileChunk(SceneNode parent, TileConfig cfg, Material material) {
		super(parent);

		int size = cfg.getChunkSize();
		int sqSize = size * size;

		mConfig = cfg;

		mTileMeshInfos = new TileMeshInfo[sqSize];

		initTiles();

		// Material material = new Material(ColorAttribute.createDiffuse(Color.WHITE));

		/*ModelBuilder mb = new ModelBuilder();
		mb.begin();
		mb.part("Tiles", mMesh, GL10.GL_TRIANGLES, material);
		Model mdl = mb.end();

		mModelInstance = new ModelInstance(mdl);

		attachRenderable(mModelInstance);*/

		setLocalBounds(size * cfg.getTileSize(), size * cfg.getTileSize(), cfg.getTileSize());
	}
	
	@Override
	public void render(ModelBatch modelBatch, Environment env) {
		
		if(mStaticGeometryMesh == null) {
			
			if(mModelInstance != null) {
				mModelInstance.model.dispose();
			}
			
			StaticGeometryBatch b = new StaticGeometryBatch();
			b.begin();
			for(TileMeshInfo tmi : mTileMeshInfos) {
				if(tmi.model != null) {
					b.add(tmi.model);
				}
			}
			mStaticGeometryMesh = b.end();
			b.dispose();
			
			Material material = new Material(ColorAttribute.createDiffuse(Color.WHITE));
			
			ModelBuilder mb = new ModelBuilder();
			mb.begin();
			mb.part("Tiles", mStaticGeometryMesh, GL20.GL_TRIANGLES, material);
			Model mdl = mb.end();		
			
			mModelInstance = new ModelInstance(mdl);
			removeAllRenderables();
			attachRenderable(mModelInstance);
		}
		
		
		super.render(modelBatch, env);
	}

	private void removeTile(TileMeshInfo tile) {
		
	}

	public void setTile(int tx, int ty, Tile t) {

		TileMeshInfo info = getTileMeshInfo(tx, ty);
		
		TileMeshInfo tmi = getTileMeshInfo(tx,ty);
		
		if(t != null)
		{
			Model mdl = t.getDescriptor().getModel();
			if(tmi.model != null && tmi.model.equals(mdl)) // Same model set.
				return;
			tmi.model = new ModelInstance(mdl);
			tmi.model.transform.setTranslation( tx * mConfig.getTileSize(), ty * mConfig.getTileSize(), 0 );
		}
		else if(tmi != null && tmi.model != null) // Remove tile
		{ 
			tmi.model = null;
		}

		if(mStaticGeometryMesh != null) { // Flag rebuild
			mStaticGeometryMesh.dispose();
			mStaticGeometryMesh = null;
		}
		

		/* Todo... */
	}

	private TileMeshInfo getTileMeshInfo(int tx, int ty) {

		int idx = ty * mConfig.getChunkSize() + tx;
		TileMeshInfo tile = mTileMeshInfos[idx];

		return tile;
	}

}
