package com.vectornoid.tiled;

import java.util.HashMap;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.vectornoid.tapdoor.gfx.scene3d.SceneNode;

/**
 * @author void
 *
 * A layer of a tiled map.
 * 
 */
public class TileLayer extends SceneNode {

	private Material mMaterial;
	private TileConfig mConfig;
	private HashMap< Integer, HashMap< Integer , TileChunk> > mChunks = new HashMap<>(); 
	
	public TileLayer(SceneNode parent, TileConfig cfg) {
		super(parent);
		mConfig = cfg;
		mMaterial = new Material(ColorAttribute.createDiffuse(Color.GREEN));		
	}
	
	private float getAbsoluteChunkSize() {
		return mConfig.getTileSize() * mConfig.getChunkSize() + 2 * mConfig.getChunkSpacing();
	}
	
	private TileChunk getChunk(int cx, int cy, boolean create) {
		
		if(!mChunks.containsKey(cy)) {
			if(!create)
				return null;
			mChunks.put(cy, new HashMap<Integer, TileChunk>());
		}
		
		HashMap<Integer, TileChunk> yMap = mChunks.get(cy);
		
		if(!yMap.containsKey(cx)) {
			if(!create) 
				return null;
			
			TileChunk chunk = new TileChunk(this, mConfig, mMaterial);
			
			float chunkSize = getAbsoluteChunkSize();
			
			chunk.setPosition(chunkSize * cx, chunkSize * cy, 0.0f);
			chunk.setRenderBoundingBox(true);
			yMap.put(cx, chunk);
			return chunk;
		}
		
		return yMap.get(cx);
		
	}
	
	public void setTile(int tx, int ty, Tile t) {
		
		int chunkdeltaX = tx % mConfig.getChunkSize();
		int chunkdeltaY = ty % mConfig.getChunkSize();
		
		int chunkX = (int) Math.floor(tx / mConfig.getChunkSize());
		int chunkY = (int) Math.floor(ty / mConfig.getChunkSize());
		
		TileChunk chunk = getChunk(chunkX, chunkY, true);
		
		chunk.setTile(chunkdeltaX, chunkdeltaY, t);
	}
	
	

}
