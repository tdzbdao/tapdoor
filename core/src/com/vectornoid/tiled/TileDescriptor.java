package com.vectornoid.tiled;

import com.badlogic.gdx.graphics.g3d.Model;

/**
 * @author void
 *
 * A tile descriptor roughly tells about the primitive type of a tile (can be a sphere, a block, a plane, ...).
 */
public abstract class TileDescriptor {
	
	abstract public Model getModel();
	
}
