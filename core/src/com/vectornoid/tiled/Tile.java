package com.vectornoid.tiled;

/**
 * @author void
 * 
 * A single tile class.
 *
 */
public class Tile {
	
	private TileDescriptor mDescriptor;

	public Tile(TileDescriptor desc) {
		mDescriptor = desc;
	}
	
	public TileDescriptor getDescriptor() {
		return mDescriptor;
	}
	
}
