package com.vectornoid.tiled;

import com.badlogic.gdx.graphics.VertexAttributes.Usage;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder;

/**
 * @author void
 * 
 * A tile descriptor that defines a block-shaped tile
 *
 */
public class BlockTileDescriptor extends TileDescriptor {

	Model mModel;
	
	public BlockTileDescriptor(TileConfig cfg) {
	
		float sz = cfg.getTileSize();
		
		ModelBuilder mb = new ModelBuilder();
		mModel = mb.createBox(sz, sz, sz, new Material(), Usage.Position | Usage.Normal | Usage.TextureCoordinates);		
	}

	@Override
	public Model getModel() {
		return mModel;
	}
	
}
