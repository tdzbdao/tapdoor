package com.vectornoid.tiled;

/**
 * @author void
 *
 * Basic configuration of the tiled map.
 *
 */
public class TileConfig {

	private int mChunkSize = 4;
	private float mTileSize = 1.0f;
	private float mChunkSpacing = -0.01f;
	
	public int getChunkSize() {
		return mChunkSize;
	}
	
	public float getTileSize() {
		return mTileSize;
	}
	
	public float getChunkSpacing() {
		return mChunkSpacing;
	}
		
}
