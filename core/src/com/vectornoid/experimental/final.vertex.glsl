varying vec3 Normal;
varying vec3 ModelVertex;
 
void main(void){
	Normal = gl_NormalMatrix * gl_Normal;
 	ModelVertex  = vec3 (gl_ModelViewMatrix * gl_Vertex);
	gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
	gl_TexCoord[0] = gl_MultiTexCoord0;
 	}