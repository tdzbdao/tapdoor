  uniform int u_renderpass;
  uniform int u_light;
  varying vec3 normal;
  varying vec3 pos;
 
  void main(void){

	gl_Position = gl_ModelViewMatrix * gl_Vertex - gl_LightSource[u_light].position ;
	float L= length (gl_Position.xyz);
	gl_Position /= -L;
	if (u_renderpass == 1) gl_Position.z *=-1.0;
	gl_Position.z += 1.0;
	gl_Position.xy /= gl_Position.z;
	if (gl_Position.z >= 0.01){
		gl_Position.z = L / 15.0;//Todo: optimize
		gl_Position.w = 1.0;
		}
	else{
		gl_Position.z = -1.0;
		gl_Position.w = -1.0;
		}
 
        gl_Position.z = 2.0 * gl_Position.z -1.0; //Todo: optimize
	pos=gl_Position.xyz;
	}