varying vec3 Normal;
varying vec3 ModelVertex;
 
uniform sampler2D u_texture0; // Eine normale Textur 
uniform sampler2D u_shadowmap;  //Damit ist auch die 4. TMU belegt..
const int MaxLights=8;
 
vec2 texofset[8]; 
void main(void){
	texofset[0] = vec2 (0.125,0.125);
	texofset[1] = vec2 (0.125,0.375);
	texofset[2] = vec2 (0.125,0.625);
	texofset[3] = vec2 (0.125,0.875);
	texofset[4] = vec2 (0.625,0.125);
	texofset[5] = vec2 (0.625,0.375);
	texofset[6] = vec2 (0.625,0.625);
	texofset[7] = vec2 (0.625,0.875);
 
        vec4 light=vec4 (0.2, 0.2, 0.2, 0.0); //Emmitiertes Licht
        vec3 normalvec =normalize(Normal.xyz);
        for (int LightNum = 0;LightNum < MaxLights; LightNum++){
                vec3 lightvec = ModelVertex - gl_LightSource[LightNum].position.xyz;
                vec3 lightdir = normalize( lightvec );
                vec2 parabol = texofset[LightNum];
                if (lightdir.z > 0.0){
                        parabol.t += 0.25;
                        }
                // parabolische Projektion f�r subtextur
                parabol -=  lightdir.xy * 0.125/ (abs (lightdir.z) + 1.0); 
                float shadow = step (length(lightvec)/15.0 -0.05, texture2D(Shadowmap, parabol).r );
                light += shadow * gl_LightSource[LightNum].diffuse * abs(dot(normalvec, lightdir));
                }
        vec4 color = texture2D(Texture0, vec2(gl_TexCoord[0])); //Textur auslesen;
       
        gl_FragColor = light * color ;
        }