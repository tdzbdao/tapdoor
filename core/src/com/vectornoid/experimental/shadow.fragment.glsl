uniform sampler2D u_shadowmap;
varying vec3 spos;
 
void main(void){
        if (texture2D(u_shadowmap, spos.xy).r > spos.z ){
              // Light
              gl_FragColor = vec4(1.0,1.0,1.0,1.0);
              }
        else{
              // Shadows
              gl_FragColor = vec4(0.5,0.5,0.5,1.0);
              }
        }