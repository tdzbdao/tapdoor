package com.vectornoid.experimental;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.vectornoid.tapdoor.gfx.FrameRenderable;

/**
 * @author void
 *
 * A rather experimental shadow renderer.
 *
 */
public class ShadowRenderer {

	private FrameBuffer mShadowMap;
	private ShaderProgram mShadowMapShader;
	private ShaderProgram mShadowGenShader;

	private PerspectiveCamera mLightCam;

	public static final int ShadowMapSize = 4096;

	public ShadowRenderer(Environment environment) {
		mShadowMap = new FrameBuffer(Format.RGBA8888, ShadowMapSize, ShadowMapSize, true);
		mLightCam = new PerspectiveCamera(67.0f, mShadowMap.getWidth(), mShadowMap.getHeight());

	/*	mShadowGenShader = new ShaderProgram(Gdx.files.classpath("com/vectornoid/experimental/shadowgen-vert.glsl").readString(), Gdx.files.classpath("com/vectornoid/experimental/shadowgen-frag.glsl").readString());
		if (!mShadowGenShader.isCompiled())
			throw new GdxRuntimeException("Couldn't compile shadow map shader: " + mShadowGenShader.getLog());
		
		mShadowMapShader = new ShaderProgram(Gdx.files.classpath("com/vectornoid/experimental/shadowmap-vert.glsl").readString(), Gdx.files.classpath("com/vectornoid/experimental/shadowmap-frag.glsl").readString());
		if (!mShadowMapShader.isCompiled())
			throw new GdxRuntimeException("Couldn't compile shadow map shader: " + mShadowMapShader.getLog());*/

		new SpriteBatch(1);
		
	}

	public void render(FrameRenderable renderable, Camera cam) {
		renderable.prepareFrame();

		mShadowMap.begin();
		Gdx.gl.glClearColor(1, 1, 1, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
		Gdx.gl.glClearColor(0, 0, 0, 0);
		Gdx.gl.glEnable(GL20.GL_CULL_FACE);
		Gdx.gl.glCullFace(GL20.GL_FRONT);
		mShadowGenShader.begin();
		mShadowGenShader.setUniformMatrix("u_projTrans", mLightCam.combined);
		
	
		
		mShadowGenShader.end();
		mShadowMap.end();
		Gdx.gl.glDisable(GL20.GL_CULL_FACE);

		mShadowMapShader.begin();
		mShadowMap.getColorBufferTexture().bind();
		mShadowMapShader.setUniformi("s_shadowMap", 0);
		mShadowMapShader.setUniformMatrix("u_projTrans", cam.combined);
		mShadowMapShader.setUniformMatrix("u_lightProjTrans", mLightCam.combined);
		mShadowMapShader.setUniformf("u_color", 1, 0, 0, 1);
	//	plane.render(mShadowMapShader, GL20.GL_TRIANGLE_FAN);
		mShadowMapShader.setUniformf("u_color", 0, 1, 0, 1);
	//	cube.render(mShadowMapShader, GL20.GL_TRIANGLES);
		mShadowMapShader.end();

	}

}
