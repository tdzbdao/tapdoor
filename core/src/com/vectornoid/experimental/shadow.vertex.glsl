varying vec3 spos;
 
void main(void){
        vec3 lightdir = gl_NormalMatrix * normalize(vec3(0.1,-1.0,0.0));
        vec3 mvertex =  vec3 (gl_ModelViewMatrix * gl_Vertex);
        vec2 pos = mvertex.xz + lightdir.xz * -mvertex.y/lightdir.y;
        pos = pos / (abs(pos)+1.0);
        pos = pos * vec2(1.0,1.8) + vec2(0.0,0.8); // Cut area behind cam.
        pos = pos * 0.5 + 0.5; // Transform to texture coords.
 
        spos.xy = pos;
        spos.z = -mvertex.y * 0.5 + 0.5 - 0.005; // far/near-clamping and anti-z-fighting offset 
 
        gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex; //Die actual transformation
 
        gl_TexCoord[0] = gl_MultiTexCoord0;
        gl_TexCoord[1] = gl_MultiTexCoord1;
}