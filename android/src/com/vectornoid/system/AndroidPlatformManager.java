package com.vectornoid.system;

import com.vectornoid.tapdoor.system.LinearTask;
import com.vectornoid.tapdoor.system.PlatformManager;
import com.vectornoid.tapdoor.system.ThreadedTask;

public class AndroidPlatformManager implements PlatformManager {

	@Override
	public void update() {
		// TODO Auto-generated method stub
		LinearTask.update();

	}

	@Override
	public Task createTask(boolean runInRenderThread) {
		// TODO Auto-generated method stub
		if (runInRenderThread)
			return new LinearTask();
		else
			return new ThreadedTask();
	}

}
