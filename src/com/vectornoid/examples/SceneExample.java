package com.vectornoid.examples;

import org.lwjgl.opengl.GL11;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.VertexAttributes.Usage;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.environment.DirectionalLight;
import com.badlogic.gdx.graphics.g3d.utils.CameraInputController;
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder;
import com.badlogic.gdx.math.Vector3;
import com.vectornoid.system.DesktopPlatformManager;
import com.vectornoid.tapdoor.gfx.scene3d.Scene;
import com.vectornoid.tapdoor.gfx.scene3d.SceneNode;
import com.vectornoid.tapdoor.system.Globals;

/**
 * 
 * The following example demonstrates the usage of the scene manager.
 * 
 * @author void (tdzbdao@gmail.com)
 *
 */

public class SceneExample implements ApplicationListener {

	/**
	 * This is an example of 3D scene usage.
	 */
	Scene scene;
	private SceneNode[] exampleNodes = new SceneNode[100];
	
	public static void main(String[] args) {
		LwjglApplicationConfiguration cfg = new LwjglApplicationConfiguration();
		cfg.title = "Scene example";
		cfg.width = 640;
		cfg.height = 480;

		new Globals(new DesktopPlatformManager());
		new LwjglApplication(new SceneExample(), cfg);
	}

	@Override
	public void create() {
		
		/**
		 * Create a simple perspective camera
		 */
		PerspectiveCamera cam = new PerspectiveCamera(67.0f, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		cam.position.set(10,10,10);
		cam.lookAt(0,0,0);
		
		cam.near = 0.1f;
		cam.far = 100.0f;
		
		Gdx.input.setInputProcessor(new CameraInputController(cam));
		
		/**
		 * Now create the scene and pass the camera.
		 */
		scene = new Scene(cam);

		scene.getEnvironment().add(new DirectionalLight().set(0.8f, 0.8f, 0.8f, -1f, -0.8f, -0.2f));
		
		/**
		 * Get a handle to the root scene node.
		 */
		SceneNode root = scene.getRootSceneNode();
		
		/**
		 * We want the bounding box to be displayed in red.
		 */
		root.setRenderBoundingBox(true);
		root.setDebugColor(Color.RED);	
		
		/**
		 * Create a simple sphere model.
		 */
		// Uncomment this line to use a box instead.
		//Model mdl = new ModelBuilder().createBox(1, 1, 1, new Material(ColorAttribute.createDiffuse(1, 0, 0, 1)), Usage.Position | Usage.Normal); 
		Model mdl = new ModelBuilder().createSphere(1, 1, 1, 10,10, GL20.GL_TRIANGLES,new Material(ColorAttribute.createDiffuse((float)Math.random(), (float)Math.random(), (float)Math.random(), (float)Math.random())), Usage.Position | Usage.Normal);
		
		for(int i=0;i<exampleNodes.length;i++) {
		
			SceneNode parent = i>0 ? exampleNodes[i-1] : root; 
			
			SceneNode n = new SceneNode(parent, "Node"+i);
			
			/** 
			 * Set the nodes local position (meaning its relative offset from its parent without rotation and transformation)
			 */
			n.setPosition(0,0.3f,0);
			n.setLocalBounds(-0.5f,-0.5f,-0.5f,0.5f,0.5f,0.5f);
			n.setScale(new Vector3(0.99f,0.99f,0.99f));
			if(i == exampleNodes.length -1 ) // Show the last nodes bounding box.
			n.setRenderBoundingBox(true);

			ModelInstance mi = new ModelInstance(mdl);
			mi.materials.get(0).set(ColorAttribute.createDiffuse( (float)Math.random(), (float)Math.random(), (float)Math.random(), (float)Math.random()) );
			
			n.attachRenderable(mi);
			
			exampleNodes[i] = n;
		}
	}

	@Override
	public void resize(int width, int height) {
		scene.getCamera().viewportWidth = width;
		scene.getCamera().viewportHeight = height;
	}
	
	@Override
	public void render() {		
			
		/**
		 * Each of the nodes will get a bit rotated around its local center
		 */
		for(int i=0;i<exampleNodes.length;i++) {
			SceneNode n = exampleNodes[i];
			n.pitch((float) Math.sin(Math.random()) * 0.03f);
			n.yaw((float) Math.cos(Math.random()) * 0.01f);
			n.roll((float) Math.sin(Math.random()) * 0.02f);
		}
				
		Gdx.gl.glClearColor(0,0,0,1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
		
		/**
		 * Scenes can be rendered multiple times, but need to prepared and ended once per frame.
		 * A useful example of rendering more than once might be the usage of different frame buffer objects.
		 */
		scene.prepareFrame();
		scene.renderFrame();
		scene.endFrame();
		
	}

	@Override
	public void pause() {
		
	}

	@Override
	public void resume() {		
	}

	@Override
	public void dispose() {
		scene.dispose(); // Dispose our scene with all its associated contents.		
	}
	
}
